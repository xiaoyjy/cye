#ifndef __MAP_HASHMAP_H__
#define __MAP_HASHMAP_H__
/**
 * hashmap struct.
 * +------------+
 * |    base    |
 * |  36bytes   |  
 * +------------+
 * |   row1     |
 * +------------+
 * |   row2     |
 * +------------+
 * |   ...      |
 * +------------+
 * |   rowN     |
 * +------------+
 *
 * base = struct S_MAP 
 * +-----------------+
 * | dwShmKey        |
 * +-----------------+
 * | dwShmSize       |
 * +-----------------+
 * | dwNodeSize      |
 * +-----------------+
 * | dwTotalSize     |
 * +-----------------+
 * | dwCurrentSize   |
 * +-----------------+
 * | dwRowCount      |
 * +-----------------+
 * | dwCurMaxRowDeep |
 * +-----------------+
 * | dwSequence      |
 * +-----------------+
 * | dwFlag          |
 * +-----------------+
 * sizeof(S_MAP) == 36bytes.
 *
 *  RowI
 * +------------+
 * | row size   |
 * +------------+
 * | HashNode 1 |
 * +------------+
 * | HashNode 2 |
 * +------------+
 * | ...        |
 * +------------+
 * | HashNode N |
 * +------------+
 * sizeof(HashNode) = sizeof(std::pair<h_key_t, h_value_t>)
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <iostream>
#include <sys/shm.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#pragma pack(1)
template <typename h_key_t, typename h_value_t>
struct S_HASH_NODE
{
	std::pair<h_key_t, h_value_t> data;
};
#pragma pack()

#define MAP_HASH_ROW_COUNT 32
#define MAP_HASH_STR_LEN 48

class hashstr_t
{
	public:
		char data[MAP_HASH_STR_LEN];

	public:
		hashstr_t()
		{
			memset(data, 0, MAP_HASH_STR_LEN);
		}

		hashstr_t(const char*p, int l)
		{
			if(l >= MAP_HASH_STR_LEN - 5)
			{
				l = MAP_HASH_STR_LEN - 5;
			}

			memset(data, 0, MAP_HASH_STR_LEN);
			memcpy(data, p, l);
		}


		hashstr_t& operator=(hashstr_t &t2)
		{
			memcpy(data, t2.data, MAP_HASH_STR_LEN);
			return *this;
		}

		hashstr_t& operator=(uint32_t t2)
		{
			data[0] = t2;
			return *this;
		}
};  

uint32_t operator%(const hashstr_t t1, uint32_t t2)
{
	/*
	   t2 will never be 0.
	   if(t2 == 0)
	   {
	   return 0;
	   }
	 */
	uint32_t t = 0;

	const char *p = t1.data;
	while((p - t1.data < MAP_HASH_STR_LEN - sizeof(unsigned int)) && *p)
	{
		t += *((unsigned int*)(p));
		p += sizeof(unsigned int);
	}


	return t%t2;
}

uint32_t operator/(const hashstr_t t1, uint32_t t2)
{
	uint32_t t = 0;

	const char *p = t1.data;
	while((p - t1.data < MAP_HASH_STR_LEN - sizeof(unsigned int)) && *p)
	{
		t += *((unsigned int*)(p));
		p += sizeof(unsigned int);
	}

	return t/t2;
}

bool operator==(const hashstr_t t1, hashstr_t t2)
{
	return strcmp(t1.data, t2.data) == 0;
}

bool operator==(const hashstr_t t1, uint32_t t2)
{
	return t1.data[0] == t2;
}

bool operator!=(const hashstr_t t1, uint32_t t2)
{
	return t1.data[0] != t2;
}


/**
 * 为了紧缩空间,可以在实例化hashmap_t时,先实例化下S_ITEM. 例如:
 *
 * #pragma pack(1)
 * static const size_t HASH_NODE_SIZE = sizeof(S_ITEM<uint32_t, xxx>);
 * #pragma pack()
 */
template <typename h_key_t, typename h_value_t>
class hashmap_t
{
	public:
		typedef S_HASH_NODE<h_key_t, h_value_t> HashNode;
#pragma pack(1)
		typedef struct
		{
			uint32_t dwRowSize;
			char     actNode[0];
		}S_ROW;

		typedef struct
		{
			uint32_t dwShmKey;
			uint32_t dwShmSize;
			uint32_t dwNodeSize;

			uint32_t dwTotalSize;
			uint32_t dwCurrentSize;
			uint32_t dwRowCount;
			uint32_t dwCurMaxRowDeep;

			uint32_t dwSequence;
			uint32_t dwFlag;

			pthread_mutex_t dwMutex;

			char data[0];
		}S_MAP;

#pragma pack()
	public:
		hashmap_t()
		{
			m_pShmAddr = NULL;
			m_pXMap    = NULL;
			m_dwShmId  = -1;

			m_dwCurCol = 0;
			m_dwCurRow = 0;

			pRowArray  = NULL;

			is_enable = false;
		}

		~hashmap_t()
		{
			if(pRowArray)
			{
				delete[] pRowArray;
			}
		}

		size_t size() const
		{
			return m_pXMap->dwCurrentSize;
		}

		size_t max_size() const
		{
			return m_pXMap->dwTotalSize;
		}

		bool empty() const
		{
			return m_pXMap->dwCurrentSize == 0;
		}

		int init(uint32_t dwShmKey, uint32_t dwRowCount, uint32_t dwShmSize);

		uint32_t info(char *info, int len);

		uint32_t reset();

		uint32_t next(h_key_t* key, h_value_t *pval);

		h_value_t* get_ptr(const h_key_t& key);

		uint32_t get(const h_key_t& key, h_value_t *pval);

		uint32_t inc(const h_key_t &key, const h_value_t& n)
		{
			return 0;
		}

		uint32_t dec(const h_key_t &key, const h_value_t& n)
		{
			return 0;
		}

		//uint32_t inc(const hashstr_t& key, const uint32_t val);
		//uint32_t dec(const hashstr_t& key, const uint32_t val);

		uint32_t del(const h_key_t& key);
		uint32_t set(const h_key_t& key, const h_value_t& val);

		void drop()
		{
			if (m_dwShmId > 0)
			{
				shmctl(m_dwShmId, IPC_RMID, NULL);
			}
		}

	private:
		uint32_t m_dwShmKey;
		uint32_t m_dwShmSize;
		uint32_t m_dwRowCount;
		uint32_t m_dwNodeSize;
		long m_dwShmId;

		uint32_t m_dwCurRow;
		uint32_t m_dwCurCol;

		bool is_enable;

		S_ROW   **pRowArray;
		S_MAP   *m_pXMap;
		void    *m_pShmAddr;
};

	template <typename h_key_t, typename h_value_t>
int hashmap_t<h_key_t, h_value_t>::init(uint32_t dwShmKey, uint32_t dwRowCount, uint32_t dwShmSize)
{
	if(_mc_G(enable) == 0)
	{
		return -1;
	}

	m_dwShmKey   = dwShmKey;
	m_dwRowCount = dwRowCount;
	m_dwShmSize  = 1024*1024*dwShmSize;
	m_dwNodeSize = sizeof(HashNode);

#if MAPCTL_DEBUG
	php_error_docref(NULL TSRMLS_CC, E_NOTICE, "hashmap_t::init: node size=%u size=%u.", m_dwNodeSize, m_dwShmSize);
#endif

	// 每行的内存长度
	uint32_t dwAveRowLength = (m_dwShmSize    - sizeof(S_MAP)) / m_dwRowCount;
	uint32_t dwAveRowSize   = (dwAveRowLength - sizeof(S_ROW)) / m_dwNodeSize;
	if (dwAveRowSize <= m_dwRowCount * 2)
	{
#if MAPCTL_DEBUG
		php_error_docref(NULL TSRMLS_CC, E_ERROR,
				"hashmap_t::init ave row size is too small. m_dwShmSize=%u, dwAveRowSize=%u, row_count=%u.", 
				m_dwShmSize,
				dwAveRowSize,
				m_dwRowCount);
#endif
		return -1;
	}

#if MAPCTL_DEBUG
	php_error_docref(NULL TSRMLS_CC, E_NOTICE,
			"hashmap_t::init: shm_size=%u, row_size=%u, col_size=%u.",
			m_dwShmSize,
			m_dwRowCount,
			dwAveRowSize);
#endif

	m_dwShmId = -1;
	bool lShmExist = true;
	if ((m_dwShmId = shmget((key_t)m_dwShmKey, m_dwShmSize, 0666)) == -1)
	{
		lShmExist = false;
		if ((m_dwShmId = shmget((key_t)m_dwShmKey, m_dwShmSize, IPC_CREAT|0666)) == -1)
		{
			php_error_docref(NULL TSRMLS_CC, E_ERROR, "hashmap_t::init: shmget failed. %s key=%u, size=%u\n",
					strerror(errno), m_dwShmKey, m_dwShmSize);

			
			exit(0);
			//return -1;
		}
	}

	m_pShmAddr = shmat(m_dwShmId, 0, 0);
	if ((char*)-1 == m_pShmAddr)
	{
		php_error_docref(NULL TSRMLS_CC, E_ERROR, "hashmap_t::init: shmat failed.");
		return -1;
	}

#if MAPCTL_DEBUG
	php_error_docref(NULL TSRMLS_CC, E_NOTICE, "hashmap_t::init: share memory addr=0x%x.", m_pShmAddr);
#endif

	m_pXMap   = (S_MAP *)m_pShmAddr;
	if (!lShmExist)
	{
#if MAPCTL_DEBUG
		php_error_docref(NULL TSRMLS_CC, E_NOTICE, "hashmap_t::init: new create share memory");
#endif

		memset(m_pXMap, 0x0, m_dwShmSize);

		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
		pthread_mutex_init(&m_pXMap->dwMutex, &attr);

		m_pXMap->dwShmKey      = m_dwShmKey;
		m_pXMap->dwShmSize     = m_dwShmSize;
		m_pXMap->dwNodeSize    = m_dwNodeSize;
		m_pXMap->dwRowCount    = m_dwRowCount;
		m_pXMap->dwTotalSize   = (dwAveRowSize-1)*m_dwRowCount;
		m_pXMap->dwCurrentSize = 0;
	}
	else
	{
#if MAPCTL_DEBUG
		php_error_docref(NULL TSRMLS_CC, E_NOTICE, "hashmap_t::init: open exists shm.");
#endif

		if ((m_pXMap->dwShmKey != m_dwShmKey)
				|| (m_pXMap->dwShmSize  != m_dwShmSize)
				|| (m_pXMap->dwNodeSize != m_dwNodeSize)
				|| (m_pXMap->dwRowCount != m_dwRowCount))
		{
#if MAPCTL_DEBUG
			php_error_docref(NULL TSRMLS_CC, E_NOTICE, "hashmap_t::init:" 
					"shmat failed, exist shmkey=%x, shmsize=%d, item size=%d, map row count =%d",
					m_pXMap->dwShmKey,
					m_pXMap->dwShmSize,
					m_pXMap->dwNodeSize,
					m_pXMap->dwRowCount);
#endif
			return -1;
		}
	}

	pRowArray = new S_ROW*[m_dwRowCount];

	char *pRowStartAddr = (char*)m_pShmAddr + sizeof(S_MAP);  
	for (int i=0; i < (int)m_dwRowCount; i++)
	{
		pRowArray[i] = (S_ROW *)(pRowStartAddr + dwAveRowLength * i);  
		pRowArray[i]->dwRowSize = dwAveRowSize - m_dwRowCount;
	}

	is_enable = true;
	return lShmExist;
}

/**
 * next.
 */
	template <typename h_key_t, typename h_value_t>
uint32_t hashmap_t<h_key_t, h_value_t>::next(h_key_t *key, h_value_t *pval)
{
	if(!pRowArray)
	{
		return 0;
	}

	HashNode *node = (HashNode*)(pRowArray[m_dwCurRow]->actNode + m_dwNodeSize * m_dwCurCol);
	while(node->data.first == 0)
	{
		m_dwCurRow = 0;
		m_dwCurCol ++;
		if(m_dwCurCol >= pRowArray[0]->dwRowSize)
		{
			return 0;
		}

		node = (HashNode*)(pRowArray[m_dwCurRow]->actNode + m_dwNodeSize * m_dwCurCol);
	}

	*key  = node->data.first;
	memcpy(pval, &(node->data.second), sizeof(h_value_t));

	m_dwCurRow++;
	if(m_dwCurRow == m_dwRowCount)
	{
		m_dwCurRow = 0;
		m_dwCurCol ++;
		if(m_dwCurCol >= pRowArray[0]->dwRowSize)
		{
			return 0;
		}
	}

	return 1;
}

/**
 * reset.
 */
	template <typename h_key_t, typename h_value_t>
uint32_t hashmap_t<h_key_t, h_value_t>::reset()
{
	m_dwCurCol = 0;
	m_dwCurRow = 0;
	return 1;
}

/**
 * 查找key,结果存到*pval中.
 * @return:
 *      0 未找到
 *      1 找到
 */
	template <typename h_key_t, typename h_value_t>
h_value_t* hashmap_t<h_key_t, h_value_t>::get_ptr(const h_key_t& key)
{
	if(!pRowArray)
	{
		return NULL;
	}

	uint32_t dwCol = 0;
	uint32_t dwRow = 0;
	for (; dwRow < m_dwRowCount; dwRow++)
	{
		dwCol = key % pRowArray[dwRow]->dwRowSize;
		HashNode *node = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);
		if (key == node->data.first)
		{
			return &(node->data.second);
		}
		else if(node->data.first == 0)
		{
			break;
		}
	}

	return NULL;
}

/**
 * 查找key,结果存到*pval中.
 * @return:
 *      0 未找到
 *      1 找到
 */
	template <typename h_key_t, typename h_value_t>
uint32_t hashmap_t<h_key_t, h_value_t>::get(const h_key_t& key, h_value_t *pval)
{
	if(!pRowArray)
	{
		return 0;
	}

	uint32_t dwCol = 0;
	uint32_t dwRow = 0;
	for (; dwRow < m_dwRowCount; dwRow++)
	{
		dwCol = key % pRowArray[dwRow]->dwRowSize;
		HashNode *node = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);
		if (key == node->data.first)
		{
			memcpy(pval, &(node->data.second), sizeof(h_value_t));
			return 1;
		}
		else if(node->data.first == 0)
		{
			break;
		}
	}

	return 0;
}

/**
 * 删除key
 * @return
 */
	template <typename h_key_t, typename h_value_t>
uint32_t hashmap_t<h_key_t, h_value_t>::del(const h_key_t& key)
{
	if(!pRowArray)
	{
		return 0;
	}

	uint32_t dwCol = 0;
	uint32_t dwRow = 0;
	for (; dwRow < m_dwRowCount - 1; dwRow ++)
	{
		dwCol = key % pRowArray[dwRow]->dwRowSize;
		HashNode *node = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);
		if (key == node->data.first)
		{
			node->data.first = 0;
			__asm__ __volatile__ ("lock; subl $1,%0" : : "m" (m_pXMap->dwCurrentSize) : "memory");
			return 1;
		}
	}

	return 0;
}

/**
 * 为int类型的值提供自增操作
 * 
 * @return
 */
	template <>
uint32_t hashmap_t<hashstr_t, uint32_t>::inc(const hashstr_t& key, const uint32_t& n)
{
	if(!pRowArray)
	{
		return 0;
	}

	uint32_t dwCol = 0;
	uint32_t dwRow = 0;
	for (; dwRow < m_dwRowCount; dwRow ++)
	{
		dwCol = key % pRowArray[dwRow]->dwRowSize;
		HashNode *node = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);
		if (key == node->data.first)
		{
			__asm__ __volatile__ ("lock; addl %0,%1" : : "ir" (n), "m" (node->data.second) : "memory");
			return 1;
		}

		if(node->data.first == 0 && this->set(key, n))
		{
			return 1;
		}
	}

	return 0;
}

/**
 * 为int类型的值提供自增减操作
 * 
 * @return
 */
	template <>
uint32_t hashmap_t<hashstr_t, uint32_t>::dec(const hashstr_t& key, const uint32_t& n)
{
	if(!pRowArray)
	{
		return 0;
	}

	uint32_t dwCol = 0;
	uint32_t dwRow = 0;
	for (; dwRow < m_dwRowCount; dwRow ++)
	{
		dwCol = key % pRowArray[dwRow]->dwRowSize;
		HashNode *node = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);
		if (key == node->data.first)
		{
			__asm__ __volatile__ ("lock; subl %0,%1" : : "ir" (n), "m" (node->data.second) : "memory");
			return 1;
		}
	}

	return 0;
}


/**
 * 添加/修改key的值为val
 * @return
 */
	template <typename h_key_t, typename h_value_t>
uint32_t hashmap_t<h_key_t, h_value_t>::set(const h_key_t& key, const h_value_t& val)
{
	if(!pRowArray)
	{
		return 0;
	}

	if (m_pXMap->dwCurrentSize >= m_pXMap->dwTotalSize)
	{
		php_error_docref(NULL TSRMLS_CC, E_WARNING,
				"hashmap_t::set share memory map has a little leavings(total=%u, current=%u)",
				m_pXMap->dwTotalSize,
				m_pXMap->dwCurrentSize);
		return 0;
	}

	bool have_lock = false;
	uint32_t dwCol = 0;
	uint32_t dwRow = 0;
	HashNode *node;
	for (; dwRow < m_dwRowCount; dwRow ++)
	{
		dwCol = key % pRowArray[dwRow]->dwRowSize;
		node  = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);

		/* 修改不需要 */
		if (key == node->data.first)
		{
			memcpy(&(node->data.second), &val, sizeof(h_value_t));
			return 1;
		}

		/* 新增需要加锁 */
		if(node->data.first == 0)
		{
			pthread_mutex_lock(&m_pXMap->dwMutex);
			have_lock = true;

			for(; dwRow < m_dwRowCount; dwRow++)
			{
				if(node->data.first == 0)
				{
					break;
				}

				dwCol = key % pRowArray[dwRow]->dwRowSize;
				node  = (HashNode*)(pRowArray[dwRow]->actNode + m_dwNodeSize * dwCol);
			}

			if(dwRow > m_pXMap->dwCurMaxRowDeep)
			{
				m_pXMap->dwCurMaxRowDeep = dwRow;
			}

			break;
		}
	}

	if(dwRow >= m_dwRowCount || !have_lock)
	{
		// hash表已经写到链表最后了，告警, 报错
		php_error_docref(NULL TSRMLS_CC, E_ERROR,
				"hashmap_t::set share memory map has a little leavings(total=%u, current=%u)",
				m_pXMap->dwTotalSize,
				m_pXMap->dwCurrentSize);

		if(have_lock)
		{
			pthread_mutex_unlock(&m_pXMap->dwMutex);
		}

		return 0;
	}

	m_pXMap->dwCurrentSize++;
	memcpy(&node->data.first , &key, sizeof(h_key_t  ));
	memcpy(&node->data.second, &val, sizeof(h_value_t));
	pthread_mutex_unlock(&m_pXMap->dwMutex);
	if((dwRow * 10) > (MAP_HASH_ROW_COUNT * 9))
	{
		// 深度达到90%，告警
		php_error_docref(NULL TSRMLS_CC, E_WARNING,
				"hashmap_t::set share memory map has a little leavings(total=%u, current=%u, current row=%u, row count=%u)",
				m_pXMap->dwTotalSize,
				m_pXMap->dwCurrentSize,
				dwRow,
				m_pXMap->dwRowCount);
	}

	return 1;
}

	template <typename h_key_t, typename h_value_t>
uint32_t hashmap_t<h_key_t, h_value_t>::info(char *info, int len)
{
	if(!pRowArray)
	{
		return 0;
	}

	int n = snprintf(info, len,
			"shm key=%u\nshm size=%uM\nnode size=%uB\ntotal slot=%u\nused slot=%u\n"
			"max deep=%u\ncurrent deep=%u\nflag=0x%x\n",
			m_pXMap->dwShmKey,
			m_pXMap->dwShmSize/1024/1024,
			m_pXMap->dwNodeSize,
			m_pXMap->dwTotalSize,
			m_pXMap->dwCurrentSize,
			m_pXMap->dwRowCount,
			m_pXMap->dwCurMaxRowDeep + 1,
			m_pXMap->dwFlag
			);

	for(int i = 0; i < m_pXMap->dwRowCount; i++)
	{
		n += snprintf(info + n, len - n, "row[%d].size=%u\n", i, pRowArray[i]->dwRowSize);
	}

	return 1;
}

#endif /*__MAP_HASHMAP_H__ */

