#ifdef HAVE_CONFIG_H

#include "config.h"
#endif

#include <dlfcn.h>

#include <php.h>
#include <main/php_variables.h>
#include <ext/standard/url.h>

#include "cy_fe.h"
#include "structs.h"
#include "ae/ae_cb.h"
#include "ae/anet.h"
#include "ae/ae.h"

#include "http.h"

const http_parser_settings* cy_http_response_settings()
{
        static const http_parser_settings settings = {
                .on_message_begin    = http_response_begin              ,
                .on_url              = http_response_on_url             ,
                .on_status           = http_response_on_status          ,
                .on_header_field     = http_response_on_header_field    ,
                .on_header_value     = http_response_on_header_value    ,
                .on_headers_complete = http_response_on_headers_complete,
                .on_body             = http_response_on_body            ,
                .on_message_complete = http_response_on_message_complete,
        };

        return &settings;
}

#define FETCH_HEAD_VAL_SUCC(name, val) \
	zend_hash_find(HASH_OF(client->headers), \
		name, sizeof(name), (void**)&val) == SUCCESS

#define CHECK_IF_FINISHED_LAST_TIME(client, which)   { \
	if(at == client->index[which].str + client->index[which].len) \
	{                                                             \
		at      = client->index[which].str;                   \
		length += client->index[which].len;                   \
	}                                                             \
	client->index[which].str = at;                                \
	client->index[which].len = length;                            \
}

int http_response_begin(http_parser *parser)
{
	cy_http_client_t     *client = parser->data;
	cy_fd_base_t         *base   = client->base;
	if(base->zresponse)
	{
		zval_ptr_dtor(&base->zresponse);
	}

	MAKE_STD_ZVAL (base->zresponse);
	object_init   (base->zresponse);

#define INIT_HTTP_VARS(name)                {    \
	if(client->rsp.name)                     \
		zval_ptr_dtor(&client->rsp.name);\
	zval *name                          ;    \
	MAKE_STD_ZVAL (name)                ;    \
	array_init    (name)                ;    \
	client->rsp.name = name             ;    \
	zend_update_property(                    \
		Z_OBJCE_P(base->zresponse),      \
		          base->zresponse ,      \
		#name, sizeof(#name)-1, name TSRMLS_CC); }

	INIT_HTTP_VARS(headers );
#undef INIT_HTTP_VARS

	return 0;
}

int http_response_on_url(http_parser *parser, const char *at, size_t length)
{
	return 0;
}

int http_response_on_status(http_parser *parser, const char *at, size_t length)
{
	return 0;
}


int http_response_on_header_field(http_parser *parser, const char *at, size_t length)
{
	cy_http_client_t *client = parser->data;
	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_HEAD_NAME);
	return 0;
}


int http_response_on_header_value(http_parser *parser, const char *at, size_t length)
{
	const char       *key                  ;
	cy_http_client_t *client = parser->data;
	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_HEAD_VALUE);

	key = estrndup(client->index[CURRENT_HEAD_NAME].str, client->index[CURRENT_HEAD_NAME].len);
	add_assoc_stringl_ex(client->rsp.headers, key, client->index[CURRENT_HEAD_NAME].len+1, (char*)at, length, 1);
	return 0;
}

int http_response_on_headers_complete(http_parser *parser)
{
	/* HTTP/1.1 or HTTP/1.0 */
	char                   version[9] = "HTTP/1.0"  ;
	zval                 **zvalue                   ;

	/* client ptr */
	cy_http_client_t      *client     = parser->data;
	cy_fd_base_t   *base       = client->base;
	cy_fd_t               *cf         = base  ->cf  ;

	/* get http version */
	version[5] = '0' + parser->http_major           ;
	version[7] = '0' + parser->http_minor           ;
	add_property_string (base->zresponse, "version", version                        , 1);
	return 0;
}

int http_response_on_body(http_parser *parser, const char *at, size_t length)
{
	if(length == 0 || parser->method != HTTP_POST)
	{
		return 0;
	}

	cy_http_client_t *client = parser->data;
	//cy_fd_t          *cf     = client->base->cf;
	//cy_cb_params_t   *fcb    = cy_fd_get_internal(cf, CY_ON_REQUEST);
	//cy_buf_token_t   *b      = fcb   ->ptr     ;

	/* check last time on body call */
	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_BODY);

	if(client->body_cb)
	{
		return (*client->body_cb)(client, at, length);
	}

	return 0;
}

int http_response_on_message_complete(http_parser *parser)
{
	cy_http_client_t    *client = parser->data ;
	cy_fd_base_t        *base   = client->base ;
	cy_fd_t             *cf     = base  ->cf   ;
	cy_buf_token_t      *b      = base  ->buf  ;

	cy_cb_params_t	    *fcb1                  ;
	cy_cb_params_t	    *fcb2                  ;

	b  ->end                    = client->index[CURRENT_BODY].str - b->buf + client->index[CURRENT_BODY].len;
	fcb1 = &cf->ccallbacks[CY_ON_REQUEST ]     ;
	fcb2 = &cf->ccallbacks[CY_ON_RESPONSE]     ;
	fcb1->args   = fcb2->args   = base  ->zresponse ;	
zend_print_zval_r(base  ->zresponse, 0);

	aeDeleteFileEvent(cf->loop, cf->fd, cf->mask   );

	// set state to "on_wait", means wait server to process.
	cf->step    = CY_EVENT_STEP_WRITEABLE ;
	cf->state   = CY_ON_RESPONSE          ;
	cf->mask    = CY_EVENT_WRITE          ;

	/* request ready, use $loop->resume($fd) to active the fd. */
	cy_ae_call_user_function_ex(cf, fcb1);
	return 0;
}

