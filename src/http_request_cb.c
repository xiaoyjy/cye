#ifdef HAVE_CONFIG_H

#include "config.h"
#endif

#include <dlfcn.h>

#include <php.h>
#include <main/php_variables.h>
#include <ext/standard/url.h>

#include "cy_fe.h"
#include "structs.h"
#include "ae/ae_cb.h"
#include "ae/anet.h"
#include "ae/ae.h"

#include "http.h"

int multipart_on_header_field   (multipart_parser* p, const char *at, size_t length);
int multipart_on_header_value   (multipart_parser* p, const char *at, size_t length);
int multipart_on_data           (multipart_parser* p, const char *at, size_t length);

int multipart_on_data_begin     (multipart_parser* p);
int multipart_on_header_complete(multipart_parser* p);
int multipart_on_data_end       (multipart_parser* p);
int multipart_on_end            (multipart_parser* p);

/*
 * struct http_parser_settings {
      http_cb      on_message_begin;
      http_data_cb on_url;
      http_data_cb on_status;
      http_data_cb on_header_field;
      http_data_cb on_header_value;
      http_cb      on_headers_complete;
      http_data_cb on_body;
      http_cb      on_message_complete;
      http_cb      on_chunk_header;
      http_cb      on_chunk_complete;
   };
 */
const http_parser_settings* cy_http_request_settings()
{
	static const http_parser_settings settings = {
		.on_message_begin    = http_request_begin              ,
		.on_url              = http_request_on_url             ,
		.on_status           = http_request_on_status          ,
		.on_header_field     = http_request_on_header_field    ,
		.on_header_value     = http_request_on_header_value    ,
		.on_headers_complete = http_request_on_headers_complete,
		.on_body             = http_request_on_body            ,
		.on_message_complete = http_request_on_message_complete,
	};

	return &settings;
}

/*
   struct multipart_parser_settings {
   multipart_data_cb on_header_field;
   multipart_data_cb on_header_value;
   multipart_data_cb on_part_data;

   multipart_notify_cb on_part_data_begin;
   multipart_notify_cb on_headers_complete;
   multipart_notify_cb on_part_data_end;
   multipart_notify_cb on_body_end;
   };
 */
static const multipart_parser_settings multipart_settings =
{
	.on_header_field      = multipart_on_header_field,
	.on_header_value      = multipart_on_header_value,
	.on_part_data         = multipart_on_data,
	.on_part_data_begin   = NULL,
	.on_headers_complete  = multipart_on_header_complete,
	.on_part_data_end     = multipart_on_data_end,
	.on_body_end          = multipart_on_end
};

#define FETCH_HEAD_VAL_SUCC(name, val) \
	zend_hash_find(HASH_OF(client->req.headers), \
			name, sizeof(name), (void**)&val) == SUCCESS

#define CHECK_IF_FINISHED_LAST_TIME(client, which)   { \
	if(at == client->index[which].str + client->index[which].len) \
	{                                                             \
		at      = client->index[which].str;                   \
		length += client->index[which].len;                   \
	}                                                             \
	client->index[which].str = at;                                \
	client->index[which].len = length;                            \
}

int http_request_init_zval(http_parser *parser)
{
	cy_http_client_t   *client = parser->data;
	cy_fd_base_t       *base   = client->base;
	if(base->zrequest)
	{
		zval_ptr_dtor(&base->zrequest);
	}

	MAKE_STD_ZVAL (base->zrequest);
	object_init   (base->zrequest);

#define INIT_HTTP_VARS(name)                {    \
	if(client->req.name)                     \
		zval_ptr_dtor(&client->req.name);\
	zval *name                          ;    \
	MAKE_STD_ZVAL (name)                ;    \
	array_init    (name)                ;    \
	client->req.name = name             ;    \
	zend_update_property(                    \
		Z_OBJCE_P(base->zrequest),       \
		          base->zrequest ,       \
		#name, sizeof(#name)-1, name TSRMLS_CC); }

	INIT_HTTP_VARS(headers );
	INIT_HTTP_VARS(cookies );
	INIT_HTTP_VARS(get     );
	INIT_HTTP_VARS(post    );
	INIT_HTTP_VARS(request );
	INIT_HTTP_VARS(servers );
	INIT_HTTP_VARS(files   );
#undef INIT_HTTP_VARS

	return 0;
}

int http_request_free_zval(http_parser *parser)
{
	cy_http_client_t   *client = parser->data;
	cy_fd_base_t       *base   = client->base;

	if(base->zrequest)
	{
		zval_ptr_dtor(&base->zrequest);
		base->zrequest = NULL;
	}

#define FREE_HTTP_VARS(name)                       \
	if(client->req.name) {                     \
		zval_ptr_dtor(&client->req.name);  \
		client->req.name = NULL;           \
	}

	FREE_HTTP_VARS(headers );
	FREE_HTTP_VARS(cookies );
	FREE_HTTP_VARS(get     );
	FREE_HTTP_VARS(post    );
	FREE_HTTP_VARS(request );
	FREE_HTTP_VARS(servers );
	FREE_HTTP_VARS(files   );

#undef FREE_HTTP_VARS

	return 0;
}

int http_request_begin(http_parser *parser)
{
	cy_http_client_t   *client = parser->data;
	cy_fd_base_t       *base   = client->base;

	http_request_init_zval(parser);

	add_property_long(base->zrequest, "request_time", time(NULL));
	return 0;
}

int http_request_on_url(http_parser *parser, const char *at, size_t length)
{
	struct http_parser_url  u                    ;
	cy_http_client_t       *client = parser->data;

	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_URL);
	http_parser_parse_url (at, length, 0, &u);
	add_assoc_stringl(client->req.servers, "uri"   , (char*)at, length, 1);
	add_assoc_stringl(client->req.servers, "query" , (char*)at + u.field_data[UF_QUERY   ].off, u.field_data[UF_QUERY   ].len, 1);
	add_assoc_stringl(client->req.servers, "path"  , (char*)at + u.field_data[UF_PATH    ].off, u.field_data[UF_PATH    ].len, 1);
	if(u.field_data[UF_QUERY].len)
	{
		return cy_treat_data(PARSE_STRING,
				at + u.field_data[UF_QUERY].off,
				     u.field_data[UF_QUERY].len,
				client->req.get, "&\0" TSRMLS_CC);
	}

	return 0;
}

int http_request_on_status(http_parser *parser, const char *at, size_t length)
{
	return 0;
}


int http_request_on_header_field(http_parser *parser, const char *at, size_t length)
{
	cy_http_client_t *client = parser->data;
	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_HEAD_NAME);
	return 0;
}


int http_request_on_header_value(http_parser *parser, const char *at, size_t length)
{
	cy_http_client_t *client = parser->data;
	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_HEAD_VALUE);

	int      size = client->index[CURRENT_HEAD_NAME].len+1;
	char key[size];

	memcpy(key, client->index[CURRENT_HEAD_NAME].str, client->index[CURRENT_HEAD_NAME].len);
	key[size] = 0;

	add_assoc_stringl_ex(client->req.headers, key, client->index[CURRENT_HEAD_NAME].len+1, (char*)at, length, 1);
	return 0;
}

int http_request_on_headers_complete(http_parser *parser)
{
	/* HTTP/1.1 or HTTP/1.0 */
	char                   version[9] = "HTTP/1.0"  ;
	zval                 **zvalue                   ;

	/* client ptr */
	cy_http_client_t      *client     = parser->data;
	cy_fd_base_t          *base       = client->base;
	cy_fd_t               *cf         = base  ->cf  ;

	/* get http version */
	version[5] = '0' + parser->http_major           ;
	version[7] = '0' + parser->http_minor           ;
	add_property_string (base->zrequest, "version", version                        , 1);
	add_property_string (base->zrequest, "method" , http_method_str(parser->method), 1);

	if(FETCH_HEAD_VAL_SUCC("Cookie", zvalue))
	{
		cy_treat_data(PARSE_COOKIE, Z_STRVAL_PP(zvalue), Z_STRLEN_PP(zvalue), client->req.cookies, ";\0" TSRMLS_CC);
	}

#define GET_SWITCH_FROM_HEADER(key, val, name)                              \
	client->name             = 0;                                       \
	if(FETCH_HEAD_VAL_SUCC(key, zvalue))                                \
	{                                                                   \
		if(strcasestr(Z_STRVAL_PP(zvalue), val) != NULL)            \
		{                                                           \
			client->name = 1;                                   \
		}                                                           \
	}                                                                   \
	add_property_long(base->zrequest, #name, client->name);             \

	GET_SWITCH_FROM_HEADER("Connection"     , "keep-alive", keepalive)
	GET_SWITCH_FROM_HEADER("Accept-Encoding", "gzip"      , gzip     )

	if(cf)
	{
		cf->keepalive = client->keepalive;
	}

#undef GET_SWITCH_FROM_HEADER

	if(FETCH_HEAD_VAL_SUCC("Content-Length", zvalue))
	{
		parser->content_length = atoi(Z_STRVAL_PP(zvalue));
	}

	/* remember body on start. */
	if(parser->method != HTTP_POST)
	{
		client->index[CURRENT_BODY].len = 0;
		client->index[CURRENT_BODY].str = client->index[CURRENT_HEAD_VALUE].str + client->index[CURRENT_HEAD_VALUE].len;
		while(*client->index[CURRENT_BODY].str == CR || *client->index[CURRENT_BODY].str == LF)
		{
			client->index[CURRENT_BODY].str++;
		}

		/* return 1 means not check body. */
		return 1;
	}
	else
	{
		/* POST data header check. and set multipart_cb */
		client->body_cb = NULL;
		if(FETCH_HEAD_VAL_SUCC("Content-Type", zvalue))
		{
			if     (strstr(Z_STRVAL_PP(zvalue), "application/x-www-form-urlencoded") != NULL)
			{
				client->body_cb = http_request_body_parse_default; 
			}
			else if(strstr(Z_STRVAL_PP(zvalue), "multipart/form-data") != NULL)
			{
				/* get boundary first */
				client->boundary = strstr(Z_STRVAL_PP(zvalue), "boundary");
				if (!client->boundary || !(client->boundary = strchr(client->boundary, '=')))
				{
					zend_error(E_WARNING, "Missing boundary in multipart/form-data POST data");
					return -1;
				}

				client->body_cb  = http_request_body_parse_rfc1867;
			}
		}
	}

	return 0;
}

int http_request_on_body(http_parser *parser, const char *at, size_t length)
{
	if(length == 0 || parser->method != HTTP_POST)
	{
		return 0;
	}

	cy_http_client_t *client = parser->data;
	//cy_fd_t          *cf     = client->base->cf;
	//cy_cb_params_t   *fcb    = cy_fd_get_internal(cf, CY_ON_REQUEST);
	//cy_buf_token_t   *b      = fcb   ->ptr     ;

	/* check last time on body call */
	CHECK_IF_FINISHED_LAST_TIME(client , CURRENT_BODY);

	if(client->body_cb)
	{
		return (*client->body_cb)(client, at, length);
	}

	return 0;
}

int http_request_on_message_complete(http_parser *parser)
{
	cy_http_client_t    *client = parser->data ;
	cy_fd_base_t        *base   = client->base ;
	cy_fd_t             *cf     = base  ->cf   ;
	cy_buf_token_t      *b      = base  ->buf  ;

	cy_cb_params_t	    *fcb1                  ;
	cy_cb_params_t	    *fcb2                  ;
	if(!cf)
	{
		return 0;
	}

	b->end     = client->index[CURRENT_BODY].str - b->buf + client->index[CURRENT_BODY].len;
	fcb1       = &cf->ccallbacks[CY_ON_REQUEST ] ;
	fcb2       = &cf->ccallbacks[CY_ON_RESPONSE] ;
	fcb1->args = fcb2->args   = base  ->zrequest ;	
zend_print_zval_r(base  ->zrequest, 0);

	aeDeleteFileEvent(cf->loop, cf->fd, cf->mask);

	// set state to "on_wait", means wait server to process.
	cf->step    = CY_EVENT_STEP_WRITEABLE ;
	cf->state   = CY_ON_RESPONSE          ;
	cf->mask    = CY_EVENT_WRITE          ;

	/* request ready, use $loop->resume($fd) to active the fd. */
	cy_ae_call_user_function_ex(cf, fcb1);
	return 0;
}


int multipart_on_header_field(multipart_parser* p, const char *at, size_t length)
{
	cy_http_client_t *client = p->data;
	return http_request_on_header_field(client->data, at, length);
}

int multipart_on_header_value(multipart_parser* p, const char *at, size_t length)
{
	cy_http_client_t *client = p->data;
	if(!client->index[CURRENT_HEAD_NAME].str)
	{
		return 1;
	}

	//form-data;
	if(strncasecmp(client->index[CURRENT_HEAD_NAME].str, "Content-Disposition",
	               client->index[CURRENT_HEAD_NAME].len) == 0)
	{
		const char *at_v;
		if((at_v = strnstr(at, "form-data; ", length)) == NULL)
		{
			return 1;
		}

		if(client->req.disposition)
		{
			zval_ptr_dtor(&client->req.disposition);
		}

		MAKE_STD_ZVAL(client->req.disposition);
		array_init   (client->req.disposition);

		at_v += sizeof("form-data; ") - 1;
		cy_treat_data(PARSE_STRING, at_v, length-(at_v-at), client->req.disposition, "&\0" TSRMLS_CC);

		zval **formname, **filename;
		if(zend_hash_find(HASH_OF(client->req.disposition), "name", 5, (void**)&formname) == FAILURE)
		{
			zval_ptr_dtor(&client->req.disposition);
			client->req.disposition = NULL;

			// Content-Disposition protocol is broken.
			return 1;
		}

		client->index[CURRENT_FORM_NAME].str = Z_STRVAL_PP(formname);
		client->index[CURRENT_HEAD_NAME].len = Z_STRLEN_PP(formname);

		// _FILES
		if(zend_hash_find(HASH_OF(client->req.disposition), "filename", 9, (void**)&filename) == SUCCESS)
		{
			zval *file;
			MAKE_STD_ZVAL(file);
			array_init   (file);

			//char *vstr;
			add_assoc_stringl_ex(file, "name"    , 5, Z_STRVAL_PP(filename), Z_STRLEN_PP(filename), 1);
			add_assoc_stringl_ex(file, "type"    , 5, "", 0, 1);
			add_assoc_stringl_ex(file, "tmp_name", 9, "content", 8, 1);
			add_assoc_long_ex   (file, "error"   , 6, 0); 

			add_assoc_zval_ex(client->req.files, Z_STRVAL_PP(formname), Z_STRLEN_PP(formname)+1, file);
			client->req.current_file_ptr = file;
		}
	}

	return 0;
}

int multipart_on_data_begin(multipart_parser* p)
{
	return 0;
}

int multipart_on_data(multipart_parser* p, const char *at, size_t length)
{
	cy_http_client_t *client = p->data;
	if(client->req.current_file_ptr == NULL && client->index[CURRENT_FORM_NAME].str)
	{
		add_assoc_stringl_ex(client->req.post,
			client->index[CURRENT_FORM_NAME].str,
			client->index[CURRENT_FORM_NAME].len+1, (char*)at, length, 1);
	}

	if(client->req.current_file_ptr)
	{
		add_assoc_stringl(client->req.current_file_ptr, "content", (char*)at, length, 1);
		client->req.current_file_ptr = NULL;
	}

	client->index[CURRENT_FORM_NAME].str = NULL;
	return 0;
}

int multipart_on_header_complete(multipart_parser* p)
{
	return 0;
}

int multipart_on_data_end(multipart_parser* p)
{
	return 0;
}

int multipart_on_end(multipart_parser* p)
{
	return 0;
}


int http_request_body_parse_default(void *p, const char *at, size_t length)
{
	cy_http_client_t *client = p;
	cy_fd_t          *cf     = client->base->cf  ;
	cy_buf_token_t   *b      = client->base->buf ;

	/* check CR & LF */
	if(b->off == (at - b->buf) + length)
	{
		/* not finished, don't process */
		return 0;
	}

	if(cy_treat_data(PARSE_STRING, at, length, client->req.post, "&\0" TSRMLS_CC) != 0)
	{
		return -1;
	}

	return 0;
}

int http_request_body_parse_rfc1867(void *p, const char *at, size_t length)
{
	cy_http_client_t *client = p;
	multipart_parser *mp     = multipart_parser_init(
			client->boundary, strlen(client->boundary), &multipart_settings);

	mp->data = client;

	size_t n = multipart_parser_execute(mp, at, length);
	if(n != length)
	{
		return -1;
	}

	return 0;
}

