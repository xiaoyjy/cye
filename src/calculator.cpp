/**
 * cy_calculator is just a calculator 
 *
 * eg:
 * int number = cy_calculator("1+2*3*(1+1)", sizeof("1+2*3*(1+1)")-1);
 * 
 * number is: 13.
 */

#include <stack>
#include <cstring>
#include "calculator.h"

static int cy_precede(char op1, char op2)//> 1,= 0,<-1
{
	switch(op1)
	{
		case '+':
		case '-':
			return (op2=='+' || op2=='-' || op2==')' || op2 == '\0') ? 1 : -1;

		case '*':
		case '/':
			return (op2=='(') ? -1 : 1;

		case '(':
			return (op2==')') ? 0 : -1;
		
		case ')':
			return 1;

		default:
			break;
	}

	return 0;
}

static int cy_compute(int num1,char op,int num2)
{
	switch(op)
	{
		case '+':
			return num1 + num2;
		case '-':
			return num1 - num2;
		case '*':
			return num1 * num2;
		case '/':
			return num1 / num2;
	}

	return 0;
}

int cy_calculator(const char* str, size_t length)
{
	long num1, num2, num;
	char op;
	std::stack<char> charsta;
	std::stack<int > intsta ;

	const char* p = str;
	while(*p != '\0' || intsta.size() > 1)
	{
		/* get numeric. */
		if(*p >= '0' && '9' >= *p)
		{
			num = 0;
			do{
				num = num*10 + (*p++ - '0');
			}
			while(*p >= '0' && '9' >= *p && p - str < length);

			intsta.push(num);
		}

		/* escape space. */
		if(*p == ' ')
		{
			goto loop_end;
		}

		/* add * before ( */
		if(*p == '(' && p != str && *(p-1) >= '0' && '9' >= *(p-1)) 
		{
			charsta.push('*');
		}

		if(charsta.size() > 0)
		{
			op = charsta.top();
			switch(cy_precede(op, *p))
			{
			case 1:
				/* Good order can be cy_compute here. */
				charsta.pop();
				num1 = intsta.top(); intsta.pop();
				num2 = intsta.top(); intsta.pop();
				intsta.push(cy_compute(num2, op, num1));
				continue;
			case -1:
				charsta.push(*p);
				break;

			case 0:
				charsta.pop();

				/* add * after ) */
				if(*p == ')' && *(p+1) >= '0' && '9' >= *(p+1))
					charsta.push('*');

				break;

			default:
				break;
			}

		}
		else
		{
			charsta.push(*p);
		}	
loop_end:
		p++;
	}

	return intsta.top();
}

