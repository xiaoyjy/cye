#include <php.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#if defined __APPLE__
 #include <libkern/OSByteOrder.h>
 #define bswap_16 OSSwapInt16
 #define bswap_32 OSSwapInt32
 #define bswap_64 OSSwapInt64
#elif defined(__OpenBSD__)
 #include <sys/endian.h>
 #define bswap_16 __swap16
 #define bswap_32 __swap32
 #define bswap_64 __swap64
#else
 #include <byteswap.h>
#endif

static const uint8_t hex2ascii[16 ] = 
{
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

static const uint8_t ascii2hex[256] =
{ 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0,
	0,10,11,12,13,14,15, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,10,11,12,13,14,15, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const char* cy_str2bin(void *dst , const char* src, size_t len)
{
	int      i                ;
	uint8_t *p = (uint8_t*)src;
	uint8_t *d = dst          ;

	for(i = 0; i < len/2; ++i)
	{
		d[i] = (ascii2hex[p[0]] << 4) + ascii2hex[p[1]];
		p   += 2;
	}

	return dst;   
}

const char* cy_str2bin_ex(void *dst , const char* src, size_t len)
{
	int      i = 0            ;
	uint8_t *p = (uint8_t*)src;
	uint8_t *d = dst          ;

	for( p = (uint8_t*)src; p - (uint8_t*)src < len - 1; )
	{
		if(ascii2hex[p[0]] && ascii2hex[p[1]])
		{
			d[i++] = (ascii2hex[p[0]] << 4) + ascii2hex[p[1]];
			p     += 2;
		}
		else
		{
			p++;
		}
	}

	return dst;   
}

const char* cy_bin2str(void *dst, char* str, size_t len)
{
	char *p = dst;
	int i;
	for(i = 0; i < len; i++)
	{
		p[0] = hex2ascii[(uint32_t)str[i]>> 4];
		p[1] = hex2ascii[(uint32_t)str[i]&0xF];
		p   += 2;
	}

	*p = '\0';
	return dst;
}

const char* cy_bin2str_ex(void *dst, char* str, size_t len)
{
	char *p = dst;
	int i;
	for(i = 0; i < len; i++)
	{
		p[0] = hex2ascii[(uint32_t)str[i]>> 4];
		p[1] = hex2ascii[(uint32_t)str[i]&0xF];
		p   += 2;

		if(i%2  == 1 ) *p++ = ' ' ;
		if(i%16 == 15) *p++ = '\n';
	}

	*p = '\0';
	return dst;
}

void cy_w_int8 (char **ptr, uint16_t v)
{
	*(*ptr)++ = v;
}

void cy_w_int16(char **ptr, uint16_t v)
{
	uint16_t sv;
	sv = htons(v);
	memcpy(*ptr, &sv, 2);
	*ptr += 2;
}

void cy_w_int32(char **ptr, uint32_t v)
{
	uint32_t iv;
	iv = htonl(v);
	memcpy(*ptr, &iv, 4);
	*ptr += 4;
}

void cy_w_int64(char **ptr, uint64_t v)
{
	uint64_t lv;
#if __BYTE_ORDER == __LITTLE_ENDIAN
	lv = bswap_64(v);
#endif
	memcpy(*ptr, &lv, 8);
	*ptr += 8;
}

unsigned long cy_r_int8 (const char **ptr)
{
	return (unsigned char)*(*ptr)++;
}

unsigned long cy_r_int16(const char **ptr)
{
	uint16_t sv;
	memcpy(&sv, *ptr, 2);
	*ptr += 2;

	return (unsigned short)ntohs(sv);
}

unsigned long cy_r_int32(const char **ptr)
{
	uint32_t iv;
	memcpy(&iv, *ptr, 4);
	*ptr += 4;

	return (unsigned int)ntohl(iv);
}

unsigned long cy_r_int64(const char **ptr)
{
	uint64_t lv;
	memcpy(&lv, *ptr, 8);
	*ptr += 8;

#if defined __x86_64__

#if __BYTE_ORDER == __LITTLE_ENDIAN
	return (unsigned long)bswap_64(lv);
#else
	return (unsigned long)lv;
#endif

#else
	return 0;
#endif

}

void cy_dump_binary(const void* data, size_t len)
{
	const unsigned char *p = (const unsigned char*)data;
	int i;
	for(i = 0; i < len; i++)
	{
		if(i%2 == 0 && i != 0)
		{
			php_printf(" ");
		}

		if(i%16 == 0)
		{
			if(i == 0)
			{
				php_printf("-----------------------------------------------\n");
				php_printf("  |\t0001 0203 0405 0607 0809 0A0B 0C0D 0E0F\n");
				php_printf("-----------------------------------------------\n00|\t");
			}
			else
			{
				php_printf("\n%02X|\t", (i/16));
			}
		}

		php_printf("%02x", p[i]);
	}

	php_printf("\n\n");
}

