#ifndef __AE_CB_H_
#define __AE_CB_H_

#include "structs.h"

int      cy_cb_do_cli_idle      (void* privdata        );
int      cy_cb_do_srv_idle      (void* privdata        );
int      cy_cb_do_connect       (void* privdata        );
int      cy_cb_do_cli_rw        (void* privdata        );
int      cy_cb_do_srv_writeable (void* privdata        );
int      cy_cb_do_cli_writeable (void* privdata        );
int      cy_cb_do_srv_readable  (void* privdata        );
int      cy_cb_do_cli_readable  (void* privdata        );
int      cy_cb_do_accept        (void* privdata        );
int      cy_cb_do_close         (void* privdata        );
int      cy_cb_do_timeout       (void* privdata        );
int      cy_cb_do_end           (void* privdata        );

int      cy_ce_do_cli_idle      (void* privdata        );
int      cy_ce_do_srv_idle      (void* privdata        );
int      cy_ce_do_connect       (void* privdata        );
int      cy_ce_do_cli_rw        (void* privdata        );
int      cy_ce_do_cli_writeable (void* privdata        );
int      cy_ce_do_srv_writeable (void* privdata        );
int      cy_ce_do_srv_readable  (void* privdata        );
int      cy_ce_do_cli_readable  (void* privdata        );
int      cy_ce_do_accept        (void* privdata        );
int      cy_ce_do_close         (void* privdata        );
int      cy_ce_do_timeout       (void* privdata        );
int      cy_ce_do_end           (void* privdata        );


int      cy_ae_call_user_function   (cy_fd_t *cf       , CY_EVENT_STATE     type);
int      cy_ae_call_user_function_ex(cy_fd_t *cf       , cy_cb_params_t    *fcb );
int      cy_ae_internal_function    (cy_fd_t *cf       , CY_EVENT_STATE     type);

void     cy_ae_do_event        (aeEventLoop     *loop  , int                fd  , void *privdata , int   mask );

/* internal set or get callback hook. */
int      cy_fd_set_ccallback   (cy_cb_params_t  *p     , cy_cb_hook_func_t *func, zval *cb       , zval *root );
int      cy_fd_set_zcallback   (cy_cb_params_t  *p     , zval *cb               , zval *args                  );

/* default fd reader. */
int      cy_fd_internal_recv   (void *param);

/* internal set or get callback hook. */
//cy_cb_params_t*
//         cy_fd_get_internal    (cy_fd_t         *cf    , CY_EVENT_STATE     type);

#endif
