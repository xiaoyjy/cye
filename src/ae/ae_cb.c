#include <php.h>

#include "cy_fe.h"
#include "ae/ae.h"
#include "ae/anet.h"
#include "ae/ae_cb.h"
#include "structs.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int cy_ae_internal_function (cy_fd_t *cf, CY_EVENT_STATE type)
{
	cy_cb_params_t *fcb = &cf->ccallbacks[type];
	if(!fcb || !fcb->func)
	{
		return 0;
	}

	return (*fcb->func)(fcb);
}

int cy_ae_call_user_function(cy_fd_t *cf, CY_EVENT_STATE type)
{
	cy_cb_params_t *fcb = &cf->zcallbacks[type];
	if(!fcb->cb)
	{
		return 0;
	}

	return cy_ae_call_user_function_ex(cf, fcb);
}

int cy_ae_call_user_function_ex(cy_fd_t *cf, cy_cb_params_t *fcb)
{
	zval                *retval          ;
	int                  ret             ;

	if(!fcb->cb)
		return 0;

	/* if args or data is not exists, use null zval */
	zval          zval_null              ;
	INIT_ZVAL    (zval_null)             ;
	zval   *null_ptr = &zval_null        ;
	zval  **params[] = { &cf->zentry,
			fcb->args ? &fcb->args : &null_ptr,
			fcb->data ? &fcb->data : &null_ptr };
	ret = call_user_function_ex(CG(function_table), NULL, fcb->cb, &retval, 3, params, 0, NULL TSRMLS_CC);
	if(ret == FAILURE)
	{
		return -2;
	}

	/* user callback function must return 0 if success */
	if(Z_TYPE_P(retval) != IS_LONG)
	{
		/* bad user return function. */
		return -3;
	}

	return Z_LVAL_P(retval);
}

int cy_cb_do_cli_idle(void* privdata)
{
	return 0;
}

int cy_cb_do_srv_idle(void* privdata)
{
	return 0;
}

/**
 * int cy_cb_do_connect(void* privdata)  check if connect is valid.
 */
int cy_cb_do_connect(void* privdata)
{
	cy_fd_t            *cf             = privdata                  ;
	struct sockaddr     addr                                       ;
	socklen_t           addr_len       = sizeof(addr)              ;
	int                 eno                                        ;

	char                ipstr[INET6_ADDRSTRLEN]                    ;
	int                 port                                       ;
	
	//char             junk;
	//eno = send(cf->fd, &junk, 0, 0L);
	//memset(&addr, 0, sizeof(struct sockaddr));

	/* get remote ip and port */
	eno = anetPeerToString(cf->fd     , ipstr        , INET6_ADDRSTRLEN, &port);
	if(eno < 0)
	{
		socklen_t result_len = sizeof(errno);
		getsockopt(cf->fd, SOL_SOCKET, SO_ERROR  , &errno    , &result_len);
		return eno;
	}

	add_property_string   (cf->zentry , "remote_addr", ipstr           , 1    );
	add_property_long     (cf->zentry , "remote_port", port                   );

	/* get local ip and port */
	eno = anetSockName    (cf->fd     , ipstr        , INET6_ADDRSTRLEN, &port);
	if(eno < 0)
	{
		return eno;
	}

	add_property_string   (cf->zentry , "local_host" , ipstr           , 1    );
	add_property_long     (cf->zentry , "local_port" , port                   );
	return 0;
}

/* not used */
int cy_cb_do_cli_writeable(void* privdata)
{
	/* connect ready, start send request */
	return 0;
}

int cy_cb_do_srv_writeable(void* privdata)
{
	return 0;
}

int cy_cb_do_cli_readable(void* privdata)
{
	DEBUG_PRINT_FUNC_START
	return 0;
}

int cy_cb_do_srv_readable(void* privdata)
{
	DEBUG_PRINT_FUNC_START
	return 0;
}

int cy_cb_do_accept(void* privdata)
{
	cy_fd_t *cf = privdata;
	int                ret    ;
	struct sockaddr   addr    ;
	socklen_t         addr_len = sizeof(struct sockaddr);

return 0;
	if((ret = accept(cf->fd, &addr, &addr_len)) < 0)
	{
		if(errno == EWOULDBLOCK)
		{
			return 0;
		}

		return -1;
	}

	return 0;
}

int cy_cb_do_close(void* privdata)
{
	cy_fd_t *cf = privdata;
	DEBUG_PRINT("at %s:%d %s\n", __FILE__,  __LINE__, __FUNCTION__);
	return 0;
}

int cy_cb_do_timeout(void* privdata)
{
	return 0;
}

int cy_cb_do_end(void* privdata)
{
	DEBUG_PRINT("at %s:%d %s\n", __FILE__,  __LINE__, __FUNCTION__);

	cy_fd_t *cf = privdata;
	aeDeleteFileEvent(cf->loop, cf->fd, CY_EVENT_READ | CY_EVENT_WRITE);
	if(cf->loop->maxfd < 0)
	{
		aeStop(cf->loop);
	}

	return 0;
}

int cy_ce_do_cli_idle(void* privdata)
{
	return 0;
}

int cy_ce_do_srv_idle(void* privdata)
{
	return 0;
}

int cy_ce_do_connect(void* privdata)
{
	cy_fd_t *cf = privdata                       ;
	cf->step    = CY_EVENT_STEP_WRITEABLE        ;
	cf->state   = CY_ON_REQUEST                  ;
	return 0;
}

int cy_ce_do_srv_writeable(void* privdata)
{
	cy_fd_t *cf = privdata;
	DEBUG_PRINT("at %s %s:%d keeplaive %d\n", __FUNCTION__, __FILE__, __LINE__, cf->keepalive);

	if(!cf->keepalive)
	{
		cf->step  = CY_EVENT_STEP_CLOSE   ;
		cf->state = CY_ON_CLOSE           ;
		cy_ae_do_event(cf->loop, cf->fd, cf, cf->mask   );
	}
	else
	{
		cf->state = CY_ON_REQUEST         ;
		cf->step  = CY_EVENT_STEP_READABLE;
	}

	return 0;
}

int cy_ce_do_srv_readable(void* privdata)
{
	cy_fd_t *cf = privdata;
	cf->step    = CY_EVENT_STEP_WRITEABLE ;
	cf->state   = CY_ON_RESPONSE          ;
	cf->mask    = CY_EVENT_WRITE          ;

	/* request ready, use $loop->resume($fd) to active the fd. */
	aeDeleteFileEvent(cf->loop, cf->fd, cf->mask   );

	DEBUG_PRINT("call %s %s:%d result %d, keepalive %d step %d state %d\n",
			__FUNCTION__, __FILE__, __LINE__, cf->result, cf->keepalive, cf->step, cf->state);
	return 0;
}

/*
 * called when finished send data to client fd, success
 *
 * cf->result always = 0
 *
 * changes status to 'ready to recvive data from server'
 */
int cy_ce_do_cli_writeable(void* privdata)
{
	DEBUG_PRINT_FUNC_START
	cy_fd_t *cf = privdata;

	/* cf->result always = 0 */
	cf->step    = CY_EVENT_STEP_READABLE ;
	cf->state   = CY_ON_RESPONSE         ;
	cf->mask    = CY_EVENT_READ          ;
	aeDeleteFileEvent   (cf->loop, cf->fd, CY_EVENT_READ|CY_EVENT_WRITE );
	if(aeCreateFileEvent(cf->loop, cf->fd, cf->mask       , cy_ae_do_event, cf) == AE_ERR)
	{
		return -100*CY_EVENT_STEP_WRITEABLE -1;
	}

	return 0;
}

/*
 * called when finished recive data from server
 * 
 * cf->result always = 0
 *
 * cf->keepalive = 0 is a 'one request -> response' connection, and the connection sould be closed immidietly 
 *
 * cf->keepalive = 1 we can't close the connection, change the status to 'ready to send data to server',
 *                   but cy_ce_do_cli_readable have no idea about 'the next writeable callback' sould implement, 
 *                   so remove events from loop 
 */
int cy_ce_do_cli_readable(void* privdata)
{
	DEBUG_PRINT_FUNC_START

	cy_fd_t *cf = privdata;
	if(!cf->keepalive)
	{
		cf->step  = CY_EVENT_STEP_CLOSE                  ;
		cf->state = CY_ON_CLOSE                          ;
		cy_ae_do_event(cf->loop, cf->fd, cf, cf->mask   );
	}
	else
	{
		cf->step    = CY_EVENT_STEP_WRITEABLE            ;
		cf->state   = CY_ON_REQUEST                      ;
		cf->mask    = CY_EVENT_WRITE                     ;

		/* we've no idea about if the next request sould be execute this time
		 * so events must be removed
		 * 
		 * use $loop->resume($fd) to active event.
		 */
		aeDeleteFileEvent   (cf->loop, cf->fd, CY_EVENT_READ|CY_EVENT_WRITE );
	}

	return 0;
}

int cy_ce_do_accept(void* privdata)
{
	return 0;
}

int cy_ce_do_close(void* privdata)
{
	DEBUG_PRINT("at %s:%d %s\n", __FILE__,  __LINE__, __FUNCTION__);

	cy_fd_t *cf = privdata;
	cf->step    = CY_EVENT_STEP_END   ;
	cf->state   = CY_ON_END           ;
	cy_ae_do_event(cf->loop, cf->fd, cf, cf->mask   );
	return 0;
}

int cy_ce_do_timeout(void* privdata)
{
	cy_fd_t *cf = privdata;

	/* do reply anyway. */
	cy_ae_call_user_function(cf, CY_ON_RESPONSE);

	/* then close the connection. */
	cf->step    = CY_EVENT_STEP_CLOSE;
	cf->state   = CY_ON_CLOSE        ;
	cy_ae_do_event(cf->loop, cf->fd, cf, cf->mask   );	
	return 0;
}

int cy_ce_do_end(void* privdata)
{
	cy_fd_t *cf = privdata;
	return 0;
}

