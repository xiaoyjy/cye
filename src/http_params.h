/*
    +--------------------------------------------------------------------+
    | PECL :: http                                                       |
    +--------------------------------------------------------------------+
    | Redistribution and use in source and binary forms, with or without |
    | modification, are permitted provided that the conditions mentioned |
    | in the accompanying LICENSE file are met.                          |
    +--------------------------------------------------------------------+
    | Copyright (c) 2004-2014, Michael Wallner <mike@php.net>            |
    +--------------------------------------------------------------------+
*/

#ifndef HTTP_PARAMS_H
#define HTTP_PARAMS_H

typedef struct http_params_token {
        char *str;
        size_t len;
} http_params_token_t;

#define HTTP_PARAMS_RAW             0x00
#define HTTP_PARAMS_ESCAPED         0x01
#define HTTP_PARAMS_URLENCODED      0x04
#define HTTP_PARAMS_DIMENSION       0x08
#define HTTP_PARAMS_RFC5987         0x10
#define HTTP_PARAMS_RFC5988         0x20
#define HTTP_PARAMS_QUERY           (HTTP_PARAMS_URLENCODED|HTTP_PARAMS_DIMENSION)
#define HTTP_PARAMS_DEFAULT         (HTTP_PARAMS_ESCAPED   |HTTP_PARAMS_RFC5987  )

typedef struct http_params_opts {
        http_params_token_t input;
        http_params_token_t **param;
        http_params_token_t **arg;
        http_params_token_t **val;
        zval *defval;
        unsigned flags;
} http_params_opts_t;

HashTable *http_params_parse(HashTable *params, const http_params_opts_t *opts TSRMLS_DC);

#endif
