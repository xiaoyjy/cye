#ifdef HAVE_CONFIG_H

#include "config.h"
#endif

#include <dlfcn.h>

#include <php.h>
#include <main/php_variables.h>
#include <ext/standard/url.h>

#include "cy_fe.h"
#include "structs.h"
#include "ae/ae_cb.h"
#include "ae/anet.h"
#include "ae/ae.h"

#include "http.h"

int cy_treat_data(int arg, const char *str, int len, zval* array_ptr, char *separator TSRMLS_DC)
{
	char *res = NULL, *var, *val;
	char *strtok_buf = NULL;
	long count = 0;
	
	res = estrndup (str, len);
	var = php_strtok_r(res, separator, &strtok_buf);	
	while (var) {
		val = strchr(var, '=');

		if (arg == PARSE_COOKIE) {
			/* Remove leading spaces from cookie names, needed for multi-cookie header where ; can be followed by a space */
			while (isspace(*var)) {
				var++;
			}
			if (var == val || *var == '\0') {
				goto next_cookie;
			}
		}

		if (++count > PG(max_input_vars)) {
			php_error_docref(NULL TSRMLS_CC, E_WARNING, "Input variables exceeded %ld. To increase the limit change max_input_vars in php.ini.", PG(max_input_vars));
			return -1;
		}

		if (val) { /* have a value */
			int val_len;
			unsigned int new_val_len;

			*val++ = '\0';
			php_url_decode(var, strlen(var));
			val_len = php_url_decode(val, strlen(val));
			add_assoc_stringl(array_ptr, var, val, val_len, 1);

			//php_register_variable_safe(var, val, new_val_len, array_ptr TSRMLS_CC);
		} else {
			int val_len;
			unsigned int new_val_len;

			php_url_decode(var, strlen(var));
			val_len = 0;
			val     = "";
			add_assoc_stringl(array_ptr, var, val, val_len, 1);
		}

next_cookie:
		var = php_strtok_r(NULL, separator, &strtok_buf);
	}

	efree(res);
	return 0;
}

