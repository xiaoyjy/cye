#ifndef __MAP_HASHTABLE_H__
#define __MAP_HASHTABLE_H__ 1

#include <stdint.h> 

#define CY_TYPE_CTL   0
#define CY_TYPE_SYS   1
#define CY_TYPE_CFG_I 2
#define CY_TYPE_LOCK  0
#define CY_TYPE_CFG_B 1

#define CY_HS_VAL_LEN 47

#pragma pack(1)

typedef struct
{
        char l;
        char v[CY_HS_VAL_LEN];
}lv;

#pragma pack()

BEGIN_EXTERN_C()

/* 初始化int型的hash */
void cy_i_init(int type);
void cy_i_drop(int type);

//void cy_ctl_init();
//void cy_ctl_drop();
uint32_t cy_i_set(const char* key, int keylen, int type, uint32_t val);
uint32_t cy_i_get(const char* key, int keylen, int type);
uint32_t cy_i_del(const char* key, int keylen, int type);
uint32_t cy_i_inc(const char* key, int keylen, int type, uint32_t val);
uint32_t cy_i_dec(const char* key, int keylen, int type, uint32_t val);

uint32_t cy_i_next(char *key, int type, uint32_t *val);
uint32_t cy_i_reset(int type);

void cy_b_init(int type);
void cy_b_drop(int type);

uint32_t cy_b_set (const char* key, int keylen, int type, lv *val);
lv*      cy_b_get (const char* key, int keylen, int type);
uint32_t cy_b_next(char *key, int type, lv *val);

uint32_t cy_b_del  (const char* key, int keylen, int type);
uint32_t cy_b_reset(int type);

uint32_t cy_ctl_succ (const char* key, int keylen, uint32_t now);
uint32_t cy_ctl_fail (const char* key, int keylen, uint32_t now);
uint32_t cy_ctl_check(const char* key, int keylen, uint32_t now);

uint32_t cy_i_info(int type, char *info, int len);
uint32_t cy_b_info(int type, char *info, int len);

END_EXTERN_C() 

#endif // __MAP_HASHTABLE_H__
