#include <php.h>
#include <Zend/zend_list.h>

#include <curl/curl.h>
#include <curl/multi.h>

#include "cy_fe.h"
#include "structs.h"

typedef struct {
        int    still_running;
        CURLM *multi;
        zend_llist easyh;
} php_curlm;

static int le_curl = 0;

#define CY_CHECK_AND_PUT_FD_LIST(type, name, ptr, fd) \
	if(FD_ISSET(fd, &name##fds)) { \
		if(!ptr) { \
			cy_object_base_t *o; \
			cf = emalloc(sizeof(cy_fd_t)); \
			memset(cf, 0, sizeof(cy_fd_t)); \
			cf->fd = fd; \
			MAKE_STD_ZVAL(ptr);  \
			object_init_ex(ptr, cy_fd_class_entry); \
			o      = zend_object_store_get_object(ptr TSRMLS_CC); \
			o->ptr = cf; \
			add_property_long(ptr, "fd"     , fd  ); \
			add_property_zval(ptr, "client" , zm  ); \
			\
			add_next_index_zval(return_value, ptr); \
		} \
		mask |= type; \
	}

PHP_FUNCTION(cy_fd_curl_multi)
{
        zval           *z_mh;
        php_curlm      *mh;
        if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "r", &z_mh) == FAILURE)
	{
                return;
        }

        fd_set          readfds;
        fd_set          writefds;
        fd_set          exceptfds;

	ZEND_GET_RESOURCE_TYPE_ID(le_curl, "curl_multi");
	if(!le_curl)
	{
		zend_error(E_ERROR, "curl_multi may not exists.");
		return;
	}

        ZEND_FETCH_RESOURCE(mh, php_curlm *, &z_mh, -1, NULL, le_curl);

	int maxfd;
	curl_multi_fdset(mh->multi, &readfds, &writefds, &exceptfds, &maxfd);
	if(maxfd < 0)
	{
		RETURN_LONG(-1);
	}

	array_init(return_value);

	zval    *zm     ;
	int      fd     ;
	int      mask   ;
	zval    *ptr    ;
	cy_fd_t *cf ;

	MAKE_STD_ZVAL (zm);
	*zm = *z_mh;
	zval_copy_ctor(zm);
	

	for(fd = 0; fd < maxfd; fd++)
	{
		cf      = NULL;
		ptr         = NULL;
		mask        = 0;
		CY_CHECK_AND_PUT_FD_LIST(CY_EVENT_READ   , read  , ptr, fd);
		CY_CHECK_AND_PUT_FD_LIST(CY_EVENT_WRITE  , write , ptr, fd);
		CY_CHECK_AND_PUT_FD_LIST(CY_EVENT_EXCEPT , except, ptr, fd);
		if(ptr)
		{
			add_property_long(ptr, "mask" , mask);
			cf->mask = mask;
		}
	}
}

