/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2012 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id: header 321634 2012-01-01 13:15:04Z felipe $ */

#ifndef PHP_CYE_H
#define PHP_CYE_H


#ifdef PHP_WIN32
#       define PHP_CYE_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#       define PHP_CYE_API __attribute__ ((visibility("default")))
#else
#       define PHP_CYE_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

PHP_MINIT_FUNCTION(cye);
PHP_MSHUTDOWN_FUNCTION(cye);

PHP_RINIT_FUNCTION(cye);
PHP_RSHUTDOWN_FUNCTION(cye);

PHP_MINFO_FUNCTION(cye);

/* cy_ctl functions */
PHP_FUNCTION(cy_ctl_check             );
PHP_FUNCTION(cy_ctl_succ              );
PHP_FUNCTION(cy_ctl_fail              );

/* shm hashmap pair [string => uint32_t] */
PHP_FUNCTION(cy_i_set                 );
PHP_FUNCTION(cy_i_init                );
PHP_FUNCTION(cy_i_drop                );
PHP_FUNCTION(cy_i_next                );
PHP_FUNCTION(cy_i_info                );
PHP_FUNCTION(cy_i_reset               );
PHP_FUNCTION(cy_i_get                 );
PHP_FUNCTION(cy_i_inc                 );
PHP_FUNCTION(cy_i_dec                 );
PHP_FUNCTION(cy_i_del                 );

/* shm hashmap pair [string => string] */
PHP_FUNCTION(cy_s_init                );
PHP_FUNCTION(cy_s_drop                );
PHP_FUNCTION(cy_s_next                );
PHP_FUNCTION(cy_s_info                );
PHP_FUNCTION(cy_s_reset               );
PHP_FUNCTION(cy_s_set                 );
PHP_FUNCTION(cy_s_get                 );
PHP_FUNCTION(cy_s_del                 );

/* shm pthread lock */
PHP_FUNCTION(cy_create_lock           );
PHP_FUNCTION(cy_lock                  );
PHP_FUNCTION(cy_unlock                );

/* hack system functions */
PHP_FUNCTION(cy_errno                 );
PHP_FUNCTION(cy_title                 );

/* cy pack or unpark, used to write raw binary protocol/ */
PHP_FUNCTION(cy_unpack                );
PHP_FUNCTION(cy_pack                  );

/* html or http parse */
PHP_FUNCTION(cy_split_by_tag          );

PHP_FUNCTION(cy_http_construct        );
PHP_FUNCTION(cy_http_parse            );


/* ae event functions */
PHP_FUNCTION(cy_ae_loop_construct     );
PHP_FUNCTION(cy_ae_add                );
PHP_FUNCTION(cy_ae_del                );
PHP_FUNCTION(cy_ae_pause              );
PHP_FUNCTION(cy_ae_resume             );
PHP_FUNCTION(cy_ae_run                );
PHP_FUNCTION(cy_ae_stop               );
PHP_FUNCTION(cy_ae_set_interval       );
PHP_FUNCTION(cy_ae_del_interval       );

/* cy_fd functions. */
PHP_FUNCTION(cy_fd_construct          );
PHP_FUNCTION(cy_fd_set_opt            );
PHP_FUNCTION(cy_fd_set_handler        );
PHP_FUNCTION(cy_fd_on                 );
PHP_FUNCTION(cy_fd_curl_multi         );
PHP_FUNCTION(cy_fd_free               );


ZEND_BEGIN_MODULE_GLOBALS(cye)

	long shm_key;
	long shm_size;
	long deep;

	long max_fail;
	long cycle;

	long enable;

	char *proc_title;

ZEND_END_MODULE_GLOBALS(cye)


ZEND_EXTERN_MODULE_GLOBALS(cye)

#ifdef ZTS
#define _mc_G(v) TSRMG(cye_globals_id, zend_cye_globals *, v)
#else
#define _mc_G(v) (cye_globals.v)
#endif

#endif  /* PHP_CYE_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
