/*
   +----------------------------------------------------------------------+
   | PHP Version 5                                                        |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997-2010 The PHP Group                                |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Author: jianyu <xiaoyjy@gmail.com>                              |
   +----------------------------------------------------------------------+
 */

/* $Id: header 297205 2010-03-30 21:09:07Z jianyu $ */

#include "cye.h"
#include "str.h"
#include "calculator.h"
#include "pack.h"

#include "structs.h"

#include <php.h>
#include <php_ini.h>
#include <ext/standard/info.h>
#include <ext/standard/php_array.h>

#include <arpa/inet.h>

#define MAX_EVAL_STRLEN  256
#define MAX_EVAL_NAMELEN 64

/**
 * eg:
 *   z  : array("number" => 5);
 *   str: "50+number"
 *   len: sizeof("50+number")
 *
 * output is:
 *   55
 */
size_t cy_eval_length(zval *z, const char* str, size_t len)
{
	/* not thread safe. */
	static char code[MAX_EVAL_STRLEN], name[MAX_EVAL_STRLEN];

	const char *p0, *p1, *pe;
	char  *pt;
	zval **zv;
	zval  *zr;
	pt = code;

	int sl, nl;

	TSRMLS_FETCH();

	if(len >= MAX_EVAL_STRLEN || len < 1)
	{
		zend_error(E_ERROR, "tag name '%s' length not between 1 and 255.", str);
		return -1;
	}

	p1 = str;
	pe = str + len;
	while(p1 != pe)
	{
		p0 = p1;
		if((*p1 >= 'a' && 'z' >= *p1) || (*p1 >= 'A' && 'Z' >= *p1) || *p1 == '_')
		{
			do
			{
				p1++;
			}
			while((*p1 >= 'a' && 'z' >= *p1) || (*p1 >= 'A' && 'Z' >= *p1) || *p1 == '_' || (*p1 >= '0' && *p1 <= '9'));

			nl = p1 - p0;
			if(p1 == pe && p0 == str)
			{
				/* no need execute string */
				if(zend_hash_find(Z_ARRVAL_P(z), (char*)p0, nl + 1, (void **)&zv) == SUCCESS
						&& Z_TYPE_PP(zv) == IS_LONG)
				{
					return Z_LVAL_PP(zv);
				}
				else
				{
					return 0;
				}
			}

			memcpy(name, p0, nl);
			name[nl] = '\0';
		}

		if(p0 == p1)
		{
			*pt++ = *p0;
			p0++;
			p1++;
		}
		else if(zend_hash_find(Z_ARRVAL_P(z), name, nl + 1, (void **)&zv) == SUCCESS
				&& Z_TYPE_PP(zv) == IS_LONG)
		{
			sl = snprintf(pt, 10, "%d", Z_LVAL_PP(zv));
			pt += sl;
			p0 = p1;
		}
		else
		{
			return 0;
		}
	}

	return cy_calculator(code, pt - code);
}

#define cy_eval_length_ex(val, z, ctl_root, zn) \
	\
if(Z_TYPE_PP(zn) == IS_STRING) \
{ \
	val = cy_eval_length(z, Z_STRVAL_PP(zn), Z_STRLEN_PP(zn)); \
	if(val == 0) \
	{ \
		val = cy_eval_length(ctl_root, Z_STRVAL_PP(zn), Z_STRLEN_PP(zn)); \
	} \
} \
else if(Z_TYPE_PP(zn) == IS_LONG) \
{ \
	val = Z_LVAL_PP(zn); \
} \
else \
{ \
	val = 0; \
}

#define UNPACK_INT_FUNCTION(base) \
	zval* unpack_uint##base(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root) \
{ \
	zval **name; \
	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&name) != SUCCESS) \
	{ \
		zend_error(E_ERROR, "int config name not found"); \
		return NULL; \
	} \
	\
	long val = (size >= base/8) ? cy_r_int##base(p) : 0;\
	add_assoc_long_ex(z, Z_STRVAL_PP(name), Z_STRLEN_PP(name) + 1, val); \
	\
	zval **is_ctl; \
	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&is_ctl) == SUCCESS) \
	{ \
		if(Z_TYPE_PP(is_ctl) == IS_LONG && Z_LVAL_PP(is_ctl) == 1) \
		{ \
			add_assoc_long_ex(ctl_root, Z_STRVAL_PP(name), Z_STRLEN_PP(name) + 1, val); \
		} \
	} \
	\
	return z; \
}

typedef zval* (*unpack_func_t)(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);

static zval* unpack_uint8 (zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_uint16(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_uint32(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_uint64(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_string(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_array (zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_object(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_objkey(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_case  (zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_cond  (zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);
static zval* unpack_tlv   (zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root);

static unpack_func_t unpack_item[17] = {
	unpack_uint8 ,
	unpack_uint16,
	unpack_uint32,
	unpack_uint64,
	unpack_string,
	unpack_array ,
	unpack_object,
	unpack_objkey,
	unpack_cond  ,
	unpack_cond  ,
	unpack_cond  ,
	unpack_cond  ,
	unpack_tlv   ,
	unpack_case  ,
	unpack_case  ,
	unpack_case  ,
	unpack_case
};

static cy_r_int pop_int[18] = {
	cy_r_int8 ,
	cy_r_int16,
	cy_r_int32,
	cy_r_int64,
	NULL,
	NULL,
	NULL,
	NULL,
	cy_r_int8 ,
	cy_r_int16,
	cy_r_int32,
	cy_r_int64,
	NULL,
	cy_r_int8 ,
	cy_r_int16,
	cy_r_int32,
	cy_r_int64,
	NULL
};

/* function unpack_uint8 */
UNPACK_INT_FUNCTION(8)

/* function unpack_uint16 */
UNPACK_INT_FUNCTION(16)

/* function unpack_uint32 */
UNPACK_INT_FUNCTION(32)

/* function unpack_uint64 */
UNPACK_INT_FUNCTION(64)

zval* unpack_string(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zt, **zv, **zl;
	size_t sl;

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS)
	{
		zend_error(E_ERROR, "string config name not found");
		return NULL;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zl) == SUCCESS)
	{
		cy_eval_length_ex(sl, z, ctl_root, zl);
	}
	else
	{
		sl = size;
	}

	if((1<<31) < sl || size < sl)
	{
		zend_error(E_WARNING, "'%s' error string length (%x) max(%x).", Z_STRVAL_PP(zv), (int)sl, (int)size);
		return NULL;
	}

	add_assoc_stringl_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, (char*)(*p), sl, 1);
	*p += sl;

	return z;
}

zval* unpack_array(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zv, **zc, **zl, **zo, *z_arr;
	size_t sl, asl, w;

	CY_Config_Type tc;
	const char *start = *p;

	if(
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zc) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 3, (void **)&zl) != SUCCESS ||
			Z_TYPE_PP(zc) != IS_LONG)
	{
		zend_error(E_ERROR, "'%s' error array type config", Z_STRVAL_PP(zv));
		return NULL;
	}

	if((tc = (CY_Config_Type)Z_LVAL_PP(zc)) > CY_TYPE_STRING)
	{
		zend_error(E_ERROR, "'%s' array or objects can not be in array.", Z_STRVAL_PP(zv));
		return NULL;
	}

	/* get array count number */
	cy_eval_length_ex(sl, z, ctl_root, zl);

	/* if string type array, compute the string length. */
	if(tc == CY_TYPE_STRING)
	{
		if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 4, (void **)&zl) != SUCCESS)
		{
			zend_error(E_ERROR, "'%s' is string array, no string length.", Z_STRVAL_PP(zv));
			return NULL;
		}

		cy_eval_length_ex(asl, z, ctl_root, zl);
	}

	w = 1 << tc;
	MAKE_STD_ZVAL(z_arr);
	array_init(z_arr);

	int i;
	for(i = 0; i != sl; i++)
	{
		if(size - (*p - start) < w)
			break; // break for loop, not switch.

		if(tc != CY_TYPE_STRING)
		{
			add_next_index_long(z_arr, (*pop_int[tc])(p));
		}
		else
		{
			add_next_index_stringl(z_arr, (char*)*p, asl, 1);
			*p += asl;
		}
	}

	add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_arr);
	return z;
}

zval* unpack_object(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zv, **zc, **zl, **zs, *z_obj;
	size_t sl, used = 0;

	const char* start = *p;
	if(
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zc) != SUCCESS ||
			Z_TYPE_PP(zc) != IS_ARRAY)
	{
		zend_error(E_ERROR, "object config error.");
		return NULL;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 4, (void **)&zs) == SUCCESS)
	{
		cy_eval_length_ex(sl, z, ctl_root, zs);

		// ignore empty length object.
		if(sl == 0)
		{
			return z;
		}

		if(sl > size)
		{
			zend_error(E_WARNING, "'%s' require (%x) but (%x) left.", Z_STRVAL_PP(zv), (int)sl, (int)size);
			goto errors;
		}

		size = sl;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 3, (void **)&zl) == SUCCESS)
	{
		cy_eval_length_ex(sl, z, ctl_root, zl);
	}
	else
	{
		if(Z_TYPE_PP(zv) == IS_STRING)
		{
			if((z_obj = unpack_output(*p, size, *zc, NULL, &used, z)) == NULL)
			{
				goto errors;
				//return NULL;
				//return NULL;
			}

			add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_obj);
		}
		else if(Z_TYPE_PP(zv) == IS_NULL)
		{
			if(unpack_output(*p, size, *zc, z, &used, z) == NULL)
			{
				goto errors;
			}
		}
		else
		{
			zend_error(E_WARNING, "invalid key.");
			goto errors;
		}

		*p += used;
		return z;
	}

	MAKE_STD_ZVAL(z_obj);
	array_init   (z_obj);

	int i;
	for(i = 0; i != sl; i++)
	{
		zval *z_sub_obj;
		if((z_sub_obj = unpack_output(*p, size - (*p - start), *zc, NULL, &used, z)) == NULL || used < 1)
		{
			zval_ptr_dtor(&z_obj);
			zend_error(E_WARNING, "unpack_output error or no out space left.");
			goto errors;
		}

		*p += used;
		add_next_index_zval(z_obj, z_sub_obj);
		if((int)(size - (*p - start)) < 2)
		{
			break;
		}
	}

	if(Z_STRVAL_PP(zv) == NULL)
	{
		zval_ptr_dtor(&z_obj);
		zend_error(E_ERROR, "%s:%d tag name can't be NULL here.", __FILE__, __LINE__);
		goto errors;
	}

	add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_obj);
	return z;

errors:
//	zend_print_zval_r(z, 0 TSRMLS_CC);
	return NULL;
}

zval* unpack_objkey(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zv, **zc, **zl, **zk, **zkv, *z_obj;
	size_t sl, used = 0;

	const char* start = *p;
	if(
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zc) != SUCCESS ||
			Z_TYPE_PP(zc) != IS_ARRAY)
	{
		zend_error(E_ERROR, "object config error.");
		return NULL;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 3, (void **)&zl) == SUCCESS)
	{
		cy_eval_length_ex(sl, z, ctl_root, zl);
	}
	else
	{
		zend_error(E_ERROR, "unkown object counter.");
		return NULL;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 4, (void **)&zk) != SUCCESS || Z_TYPE_PP(zk) != IS_STRING)
	{
		zend_error(E_ERROR, "object key config error.");
		return NULL;
	}

	MAKE_STD_ZVAL(z_obj);
	array_init(z_obj);
	
	int i;
	for(i = 0; i != sl; i++)
	{
		zval *z_sub_obj;
		if((z_sub_obj = unpack_output(*p, size - (*p - start), *zc, NULL, &used, z)) == NULL || used < 1)
		{
			zval_ptr_dtor(&z_obj);
			zend_error(E_WARNING, "unpack_output error or no out space left.");
			return NULL;
		}

		if(zend_hash_find(Z_ARRVAL_P(z_sub_obj), Z_STRVAL_PP(zk), Z_STRLEN_PP(zk) + 1, (void**)&zkv) != SUCCESS)
		{
			zval_ptr_dtor(&z_obj);
			zend_error(E_WARNING, "unpack_output key %s, but not found in child struct", Z_STRVAL_PP(zk));
			return NULL;
		}

		*p += used;
		if(Z_TYPE_PP(zkv) == IS_STRING)
		{
			add_assoc_zval_ex(z_obj, Z_STRVAL_PP(zkv), Z_STRLEN_PP(zkv), z_sub_obj);
		}
		else if(Z_TYPE_PP(zkv) == IS_LONG)
		{
			add_index_zval(z_obj, Z_LVAL_PP(zkv), z_sub_obj);
		}
		else
		{
			zval_ptr_dtor(&z_obj);
			zend_error(E_WARNING, "unpack_output key %s, but child key not be string or integer. ", Z_STRVAL_PP(zk));
			return NULL;
		}

		if((int)(size - (*p - start)) < 2)
		{
			break;
		}
	}

	add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_obj);
	return z;
}

zval* unpack_tlv(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zv, **zn, **zs, **zt, **zl, **zc, **zo, **zi, *z_tlv, *z_dst;
	size_t used;

	int i = 0;
	long type, length, cl = -1;

	const char* start = *p;

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS)
	{
		zend_error(E_ERROR, "tlv config name not found");
		return NULL;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zt) != SUCCESS ||
			Z_TYPE_PP(zt) != IS_LONG)
	{
		zend_error(E_ERROR, "'%s' unknown tlv type.", Z_STRVAL_PP(zv));
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 3, (void **)&zl) != SUCCESS ||
			Z_TYPE_PP(zl) != IS_LONG)
	{
		zend_error(E_ERROR, "'%s' unknown tlv length.", Z_STRVAL_PP(zv));
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 4, (void **)&zc) != SUCCESS ||
			Z_TYPE_PP(zc) != IS_ARRAY)
	{
		zend_error(E_ERROR, "'%s' unknown tlv config.", Z_STRVAL_PP(zv));
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 5, (void **)&zn) == SUCCESS)
	{
		cy_eval_length_ex(cl, z, ctl_root, zn);
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 6, (void **)&zs) == SUCCESS)
	{
		cy_eval_length_ex(size, z, ctl_root, zs);
	}

	if(cl == 0 || size == 0)
	{
		return z;
	}

	if(Z_TYPE_PP(zv) == IS_STRING)
	{
		MAKE_STD_ZVAL(z_tlv);
		array_init(z_tlv);
		add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_tlv);
		z_dst = z_tlv;
	}
	else
	{
		z_dst = z;
	}

	/* start unpack tlv */
type_tlv:
	type   = (*pop_int[Z_LVAL_PP(zt)])(p);
	length = (*pop_int[Z_LVAL_PP(zl)])(p);
	if(length > size)
	{
		zend_error(E_WARNING, "'%s' tlv length error, type(%x) length(%x).",
				Z_STRVAL_PP(zv), (int)type, (int)length);
		return NULL;
	}

	if(zend_hash_index_find(Z_ARRVAL_PP(zc), type, (void **)&zo) == SUCCESS)
	{
		if(zend_hash_index_find(Z_ARRVAL_PP(zo), 0, (void **)&zi) != SUCCESS ||
				Z_TYPE_PP(zi) != IS_LONG)
		{
			zend_error(E_ERROR, "'%s' unknown tlv config, type(%x).", Z_STRVAL_PP(zv), (int)type);
			return NULL;
		}

		if((*unpack_item[Z_LVAL_PP(zi)])(z_dst, zo, p, length, ctl_root) == NULL)
		{
			zend_error(E_WARNING, "'%s' tlv error, type(%x) length(%x).",
					Z_STRVAL_PP(zv), (int)type, (int)length);
			return NULL;
		}
	}
	else
	{
		// ignore the type not defined.
		*p += length;
	}

	if(size > *p - start && cl != ++i)
	{
		goto type_tlv;
	}

	return z;
}

zval* unpack_case(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zt, **zv, **zc, **zl, **zo, *z_cond, *z_dst;
	size_t sl, used = 0;
	long cond;

	const char *start = *p;
	int i = 0;

	if(
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 0, (void **)&zt) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zc) != SUCCESS ||
			Z_TYPE_PP(zc) != IS_ARRAY)
	{
		zend_error(E_ERROR, "'%s', no condition config.", Z_STRVAL_PP(zv));
		return NULL;
	}

	/* thrid argment is length. */
	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 3, (void **)&zl) == SUCCESS)
	{
		cy_eval_length_ex(sl, z, ctl_root, zl);
		if(sl == 0)
		{
			return z;
		}
	}

	if(Z_TYPE_PP(zv) == IS_STRING)
	{
		MAKE_STD_ZVAL(z_cond);
		array_init(z_cond);
	}

type_cond:
	cond = (*pop_int[Z_LVAL_PP(zt)])(p);
	if(
			zend_hash_index_find(Z_ARRVAL_PP(zc), cond       , (void **)&zo) != SUCCESS &&
			zend_hash_index_find(Z_ARRVAL_PP(zc), CY_COMMON_COND, (void **)&zo) != SUCCESS)
	{
		zend_error(E_WARNING, "config cond %ld, not found.", cond);
		return NULL;
	}

	if((z_dst = unpack_output(*p, size - (*p - start), *zo, NULL, &used, z)) == NULL)
	{
		zend_error(E_WARNING, "'%s' cond(%d) error.", Z_STRVAL_PP(zv), (int)cond);
		return NULL;
	}
	*p += used;

	if(Z_TYPE_PP(zv) == IS_STRING)
	{
		add_index_zval(z_cond, cond, z_dst);
		if(sl != 0 && ++i != sl && size - (*p - start) > 1)
		{
			goto type_cond;
		}

		add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_cond);
	}
	else
	{
		add_index_zval(z, cond, z_dst);
	}

	return z;
}

zval* unpack_cond(zval *z, zval **cfg, const char** p, size_t size, zval *ctl_root)
{
	zval **zt, **zv, **zc, **zl, **zo, *z_cond, *z_dst;
	size_t sl, used = 0;
	long cond;

	const char *start = *p;
	int i = 0;

	if(
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 0, (void **)&zt) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 1, (void **)&zv) != SUCCESS ||
			zend_hash_index_find(Z_ARRVAL_PP(cfg), 2, (void **)&zc) != SUCCESS ||
			Z_TYPE_PP(zc) != IS_ARRAY)
	{
		zend_error(E_ERROR, "'%s', no condition config.", Z_STRVAL_PP(zv));
		return NULL;
	}

	/* thrid argment is length. */
	if(zend_hash_index_find(Z_ARRVAL_PP(cfg), 3, (void **)&zl) == SUCCESS)
	{
		cy_eval_length_ex(sl, z, ctl_root, zl);
		if(sl == 0)
		{
			return z;
		}
	}

	if(Z_TYPE_PP(zv) == IS_STRING)
	{
		MAKE_STD_ZVAL(z_cond);
		array_init(z_cond);
	}

	if(Z_TYPE_PP(zc) == IS_NULL)
	{
		cond = (*pop_int[Z_LVAL_PP(zt)])(p);
		add_assoc_long_ex(z, "errno", sizeof("errno"), cond);
		return z;
	}

type_cond:
	cond = (*pop_int[Z_LVAL_PP(zt)])(p);
	if(
			zend_hash_index_find(Z_ARRVAL_PP(zc), cond          , (void **)&zo) != SUCCESS &&
			zend_hash_index_find(Z_ARRVAL_PP(zc), CY_COMMON_COND, (void **)&zo) != SUCCESS)
	{
		zend_error(E_WARNING, "config cond %ld, not found.", cond);
		return NULL;
	}

	if((z_dst = unpack_output(*p, size - (*p - start), *zo, NULL, &used, z)) == NULL)
	{
		zend_error(E_WARNING, "'%s' cond(%d) error.", Z_STRVAL_PP(zv), (int)cond);
		return NULL;
	}
	*p += used;

	if(Z_TYPE_PP(zv) == IS_STRING)
	{
		if(0 == sl)
		{
			add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_dst);
			return z;
		}

		add_index_zval(z_cond, cond, z_dst);
		if(++i != sl && size - (*p - start) > 1)
		{
			goto type_cond;
		}

		add_assoc_zval_ex(z, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv) + 1, z_cond);
	}
	else
	{
		add_assoc_long_ex(z, "errno", sizeof("errno"), cond );
		add_assoc_zval_ex(z, "data" , sizeof("data" ), z_dst);
	}

	return z;
}

/**
 * main unpack function.
 *
 *
 */
zval* unpack_output(const char* output, size_t size, zval *config, zval *dest, size_t *used, zval *ctl_root)
{
	zval *z, **cfg, **zt, *zp;

	const char *p = output;
	if(dest != NULL)
	{
		z = dest;
	}
	else
	{
		MAKE_STD_ZVAL(z);
		array_init(z);
	}

	zval *ctl = NULL;
	if(ctl_root == NULL)
	{
		MAKE_STD_ZVAL(ctl);
		array_init(ctl);
		ctl_root = ctl;
	}

	CY_Config_Type tc;
	if(Z_TYPE_P(config) != IS_ARRAY)
	{
		return z;
	}

	/* reused config must reset internal pointer. */
	HashTable *ht = Z_ARRVAL_P(config);
	zend_hash_internal_pointer_reset(ht);
	while(zend_hash_get_current_data(ht, (void **) &cfg) == SUCCESS)
	{
		zend_hash_move_forward(ht);

		if(
				Z_TYPE_PP(cfg) !=  IS_ARRAY ||
				zend_hash_index_find(Z_ARRVAL_PP(cfg), 0, (void **)&zt) != SUCCESS ||
				Z_TYPE_PP(zt) != IS_LONG
		  ){
			continue;
		}

		if(Z_LVAL_PP(zt) > CY_TYPE_CASE64 || Z_LVAL_PP(zt) < 0)
		{
			zend_error(E_WARNING, "config type not supported.");
			goto errors;
		}

		if((p - output) > size)
		{
			if(used == NULL)
			{
				// å¦‚æžœè¦æ±‚è¿”å›žç”¨äº†å¤šå°‘ï¼Œå°±ä¸æŠ¥é”™äº†
				zend_error(E_WARNING, "package size checksum failed, length:%d, buffer size: %u",
						(int)(p - output), (unsigned int)size);
			}

			break;
		}

		if((*unpack_item[Z_LVAL_PP(zt)])(z, cfg, &p, size - (p - output), ctl_root) == NULL)
		{
			goto errors;
		}
	}

	if(ctl != NULL)
	{
		zval_ptr_dtor(&ctl);
	}

	if(used != NULL) *used = p - output;
	return z;

errors:

#if ZEND_DEBUG
	zend_print_zval_r(z, 0 TSRMLS_CC);
#endif

	if(ctl != NULL)
	{
		zval_ptr_dtor(&ctl);
	}

	if(z != dest)
	{
		zval_ptr_dtor(&z);
	}

	return NULL;
}

const char* pack_input(zval *inputs, char* inbuf, size_t* inlen)
{
	zval **entry, **zt, **zv;
	size_t sl, size;

	char *p = inbuf;
	size = *inlen;

#if defined __GNUC__
	/* label value operator.  */
	/* INIT goto address map. */
	static void* goto_ptr[7] = { NULL };
	if(goto_ptr[CY_TYPE_UINT8] == NULL)
	{
		goto_ptr[CY_TYPE_UINT8 ] = &&type_uint8 ;
		goto_ptr[CY_TYPE_UINT16] = &&type_uint16;
		goto_ptr[CY_TYPE_UINT32] = &&type_uint32;
		goto_ptr[CY_TYPE_UINT64] = &&type_uint64;
		goto_ptr[CY_TYPE_STRING] = &&type_string;
		goto_ptr[CY_TYPE_ARRAY ] = &&type_array ;
		goto_ptr[CY_TYPE_OBJECT] = &&type_object;
	}

#endif // __GNUC__

	HashTable *ht = Z_ARRVAL_P(inputs);
	zend_hash_internal_pointer_reset(ht);
	while(zend_hash_get_current_data(ht, (void **) &entry) == SUCCESS)
	{
		zend_hash_move_forward(ht);

		if(Z_TYPE_PP(entry) ==  IS_ARRAY &&
				zend_hash_index_find(Z_ARRVAL_PP(entry), 0, (void **)&zt) == SUCCESS &&
				zend_hash_index_find(Z_ARRVAL_PP(entry), 1, (void **)&zv) == SUCCESS &&
				Z_TYPE_PP(zt) == IS_LONG
		  ){
			if((p - inbuf) + 9 > size) //check overflow.
			{
				zend_error(E_ERROR, "input size %d, max is %u", (int)(p - inbuf), (unsigned int)size);
				return NULL;
			}

			if((Z_LVAL_PP(zt) > CY_TYPE_OBJECT && Z_LVAL_PP(zt) != CY_TYPE_TLV) || Z_LVAL_PP(zt) < 0)
			{
				zend_error(E_ERROR, "input type not supported.");
				return NULL;
			}

#if defined __GNUC__
			goto *goto_ptr[Z_LVAL_PP(zt)];

type_uint8:
			if(Z_TYPE_PP(zv) == IS_STRING) convert_to_long(*zv);
			*p++ = Z_LVAL_PP(zv);
			continue;

type_uint16:
			if(Z_TYPE_PP(zv) == IS_STRING) convert_to_long(*zv);
			cy_w_int16(&p, Z_LVAL_PP(zv));
			continue;

type_uint32:
			//convert_to_long(*zv);
			if(Z_TYPE_PP(zv) == IS_STRING) convert_to_long(*zv);

#if defined __x86_64__
			cy_w_int32(&p, Z_LVAL_PP(zv));
#else
			if(Z_TYPE_PP(zv) != IS_LONG)
			{
				cy_w_int32(&p, (long long)Z_DVAL_PP(zv));
			}
			else
			{
				cy_w_int32(&p, Z_LVAL_PP(zv));
			}
#endif
			continue;

type_uint64:
#if defined __x86_64__
			//convert_to_long(*zv);
			if(Z_TYPE_PP(zv) == IS_STRING) convert_to_long(*zv);
			cy_w_int64(&p, Z_LVAL_PP(zv));
			continue;
#else  // __x86_64__
			zend_error(E_ERROR, "non 64bit system, 64 bit not supported.");
			return NULL;
#endif //  __x86_64__

type_string:
			if(Z_TYPE_PP(zv) == IS_STRING)
			{
				cy_w_buffer(p, Z_STRVAL_PP(zv), Z_STRLEN_PP(zv));
			}
			else
			{
				zend_error(E_ERROR, "unkown string type value.");
				return NULL;
			}

			continue;

type_array:
type_object:
			if(Z_TYPE_PP(zv) != IS_ARRAY)
			{
				zend_error(E_ERROR, "unkown array type inputs.");
				return NULL;
			}

			sl = size - (p - inbuf);
			if(pack_input(*zv, p, &sl) == NULL)
				return NULL;

			p += sl;
			continue;

#else // _GNUC__
			zend_error(E_ERROR, "non __GNUC__ is not supported. ");
#endif // end __GNUC__
		}

	}

	*inlen = p - inbuf;
	return inbuf;
}

PHP_FUNCTION(cy_pack)
{
	char data[CY_BUFFER_LENGTH];
	size_t size = CY_BUFFER_LENGTH;
	zval *inputs;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"z", &inputs) == FAILURE)
	{
		RETURN_FALSE;
	}

	if(pack_input(inputs, data, &size) == NULL)
	{
		RETURN_FALSE;
	}

	RETURN_STRINGL(data, size, 1);
}

PHP_FUNCTION(cy_unpack)
{
	const char *data;
	int  size;
	zval *config;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sz", &data, &size, &config) == FAILURE)
	{
		RETURN_FALSE;
	}

	zval *z;
	if((z = unpack_output(data, size, config, NULL, NULL, NULL)) == NULL)
	{
		RETURN_FALSE;
	}

	RETURN_ZVAL(z, 0, 1);
}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
