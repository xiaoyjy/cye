#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "php.h"
#include "cy_fe.h"

#define cy_is_w(n) \
	((64 < n && n < 91) || (96 < n && n < 123) || (47 < n && n < 58))

static inline char* skip_quote(char* p, char* e, char quote)
{
	int status = 0;
	char *head = p;

	if(*p == quote) 
	{
		do
		{
			if(*p == '>' && status == 0)
			{
				status = 1;
			}

			// error check...
			if(status)
			{
				if(status == 1 && *p == '<')
				{
					status = 2;
				}
				else if(status == 2 && *p == '>')
				{
					// bad quote just skip it.
					return head + 1;
				}
			}

			p += (*p == '\\') ? 2 : 1;
		}
		while(p < e && *p != quote);

		if(p == e)
		{
			return head + 1;
		}

	}

	return p;
}

static char* find_end_tag(char* p, char* e, char *tag, int taglen)
{
	int child = 0;	
	char c;

	while(1)
	{
		/* find first start tag '<' */
		while(p < e && *p++ != '<');
		/*
		while(p < e && *p++ != '<')
		{
			p = skip_quote(p, e, '"' );
			p = skip_quote(p, e, '\'');
		}
		*/

		/* sizeof('</'+tag+'>') == taglen + 3, that's why 3. */
		if(e - p < taglen + 3) return NULL;

		/* check have sub tags. */	
		if(strncasecmp(p, tag, taglen) == 0)
		{
			c = *(p + taglen);
			if(!cy_is_w(c))
			{
				child++;
				p += taglen;
			}
		}

		/* just find a close tag, if child count is 0, we the one we just end. */
		else if(*p == '/' && strncasecmp(p+1, tag, taglen) == 0)
		{
			if(child == 0)
			{
				return p + taglen + 3/* sizeof('</'+tag+'>') == taglen + 3*/;
			}

			child--;
			p += taglen + 1;
		}
	}
}


//char** cy_split_by_tag(char* html, int length, char** tags, int count)
PHP_FUNCTION(cy_split_by_tag)
{
	int i = 0, k, w, l, skip, size, equal = 0;
	char
		*p /* pointer */,
		*b /* begin */, 
		*e /* end */ ,
		*m,
		*n, /* m,n is tmp variable. */ 
		*s;

	HashTable *htg;
	zval *tags, **tag;

	if(
			zend_parse_parameters_ex(ZEND_PARSE_PARAMS_QUIET, ZEND_NUM_ARGS() TSRMLS_CC,
				(char*)"s|a", &s, &size, &tags) == FAILURE)
	{
		RETURN_BOOL(0); 
	}	

	p = s;
	b = s;
	e = s + size;

	array_init(return_value);

begin:

	/* NOTICE: p just at '|' */
	/**
	 *  Find out:
	 *         xxxx|<foo>
	 *         xxxx|<foo ...>
	 *     or  xxxx|</foo>
	 *
	 *  but ignore:
	 *         xxxx|<html> end of xxxx will be ignored.
	 */
	while(p < e && *p != '<')
	{
		//p = skip_quote(p, e, '"');
		//p = skip_quote(p, e,'\'');
		p++;
	}

	/* trim \r \n \s. */
	for(n = p - 1; n - b > 0 && (*n == ' ' || *n == '\n' || *n == '\r' || *n == '\t'); n--);

	if(p!=b)
	{
		/* append new line. */
		add_index_stringl(return_value, (b - s), b, n - b + 1, 1);
	}

	/***
	 * Last html tag
	 *
	 * may be:
	 *     </html>|\r\n
	 *
	 */
	if(p + 3 /* tag size must > 3 */ > e)
	{
		goto end;
	}

	// bad close tag, skip it.
	if(*(p+1) == '/')
	{
		while(p < e && *++p != '<');
		b = p; goto begin;
	}

	/**
	 * Then:
	 *  b: |<foo>xxxx or |</foo>xxxx or |<foo ...> or |</html>
	 *  p: <foo|>xxxx or </foo|>xxxx or <foo ...|> or </html|>
	 */
	for(b = p; p < e && *p != '>'; p++)
	{
		/* TODO: */
		/* bad case: <b> < </b> */
		/* bad case: <!--<span> <span> xxx --> */
		if(*p == '=')
		{
			equal = 1;
		}

		if(equal)
		{
			p = skip_quote(p, e, '"' );
			p = skip_quote(p, e, '\'');
			if(*p != '=' && *p != ' ' && *p != '\t' && *p != '\r' && *p != '\n' && *p != '\'' && *p != '"')
			{
				equal = 0;
			}
		}
	}

	equal = 0;

	/* no tag size < 2 */
	if(p + 2 > e)
	{
		// log Invalid html
		//printf("error unclose '>' tag.\n");
		goto end;
	}

	/* p: <foo>|xxxx or </foo>|xxxx or <foo ...>| or </html>| */
	p++; 

	/* tag name will be [m -> n] */
	w = *(b+1) == '/' ? 2 : 1;
	m = b + w;
	n = m; while(n && cy_is_w(*n)) n++;

	/**
	 * n: <foo|> or <foo|....> */
	l = n - m;

	skip = 0;

	/* start check tag hits, get tag name first. */
	if(tags != NULL)
	{
		htg = Z_ARRVAL_P(tags);
		zend_hash_internal_pointer_reset(htg);
		while(zend_hash_get_current_data(htg, (void**)&tag) == SUCCESS)
		{
			zend_hash_move_forward(htg);
			if(Z_TYPE_PP(tag) != IS_STRING)
			{
				continue;
			}

			if(l == Z_STRLEN_PP(tag) && strncasecmp(Z_STRVAL_PP(tag), m, l) == 0)
			{
				skip = 1;
				break;
			}
		}
	}

	/* pass through the tag if no hit. */
	if(!skip) { b = p; goto begin; }

	/**
	 *  Hit skip tag, right now
	 *
	 *  b: |<foo>xxxx or |</foo>xxxx or <foo ...|> or |</html>
	 *  p: <foo|>xxxx or </foo|>xxxx or <foo ...|> or </html|>
	 *  m: <|foo>xxxx or </|foo>xxxx or <|foo ...> or </|html>
	 *  n: <foo|>xxxx or </foo|>xxxx or <foo| ...> or </html|>
	 *
	 *  so n - m == 'foo'
	 */
	if(l == 1 && (*m == 'p' || *m == 'P'))
	{
		/**
		 * Hit <p> tag. 
		 *
		 * <p> is a very speical tag
		 * most of web editors support <p>, contents in <p> should not be separated.
		 */
		if(w == 2)
		{
			//printf("found </p> without <p>, skip it.\n");
			// shoud not happen.
			p++
				/* strlen('</p>') */;                                                                   
			goto begin;   	
		}


		/* p: <p>|xxxx or <p xxxxx>|....  */
		if((n = find_end_tag(p, e, "p", 1)) == NULL)
		{
			/* Invlid html, tag <p> is not closed, skip the first <p>, and try again. */
			goto begin;
		}

		p = n;
	}

	goto begin;

end:
	;
}

