#ifndef __CY_H__
#define __CY_H__

#include "cy_fe.h"

#define DEBUG_PRINT(fmt, args...) printf(fmt, ##args)
#define DEBUG_PRINT_FUNC_START    DEBUG_PRINT("call %s at %s:%d, pid:%d\n", __FUNCTION__, __FILE__,  __LINE__, getpid());

#define CY_BUF_TOKEN_MAX_SIZE 16*1024*1024

//#define CY_BUF_INITIA_LEN 8192
#define CY_BUF_INITIA_LEN 64

typedef void (cy_cb_free_func_t     ) (void*                     );
typedef int  (cy_cb_hook_func_t     ) (void*                     );

typedef enum _CY_EVENT_STATE
{
	CY_ON_REQUEST          = 0,
	CY_ON_RESPONSE         = 1,
	CY_ON_IDLE             = 2,
	CY_ON_CONNECT          = 3,
	CY_ON_ACCEPT           = 4,
	CY_ON_CLOSE            = 5,
	CY_ON_TIMEOUT          = 6,
	CY_ON_ERROR            = 7,
	CY_ON_READY            = 8,
	CY_ON_END              = 9,
	CY_ON_MAX              = 10

} CY_EVENT_STATE;


typedef enum _CY_EVENT_STEP
{
	CY_EVENT_STEP_IDLE           = 0,
	CY_EVENT_STEP_CONNECT        = 1,
	CY_EVENT_STEP_ACCEPT         = 2,
	CY_EVENT_STEP_CLOSE          = 3,
	CY_EVENT_STEP_READABLE       = 4,
	CY_EVENT_STEP_WRITEABLE      = 5,
	CY_EVENT_STEP_TIMEOUT        = 6,
	CY_EVENT_STEP_END            = 7,
	CY_EVENT_STEP_MAX            = 8

} CY_EVENT_STEP ;

typedef enum _CY_FD_TYPE
{
	CY_FD_TCP                    = 0,
	CY_FD_UDP                    = 1,
	CY_FD_UNIX_SOCK              = 2,
	CY_FD_FILE                   = 3

} CY_FD_TYPE ;

typedef struct aeEventLoop        aeEventLoop        ;
typedef struct _cy_fd_t           cy_fd_t            ;
typedef struct _cy_fd_base        cy_fd_base_t       ;

typedef struct _cy_cb_params_t {

	/* internal callback function */
	cy_cb_hook_func_t *func;

	/* input params */
	zval              *args;

	/* used by output params */
	zval              *data;

	/* user defined callback function */
	zval              *cb  ;

	/* pointer to cy_fd_t */
	cy_fd_t           *cf  ;

	/* zval root */
	zval              *root;

	/* hook pointer */
	void              *ptr ;
	
} cy_cb_params_t ;

typedef struct _cy_fd_t {
	int                  fd          ;
	long                 mask        ;

	/* user callback function return code. */
	int                  result      ;

	/* 2 server accept, 1 server fd or  0 client */
	int                  type        ;

	/* zentry is pointer of zend cy_fd */
	zval                *zentry      ;

	CY_EVENT_STATE       state       ;
	CY_EVENT_STEP        step        ;

	cy_cb_params_t       ccallbacks[CY_ON_MAX];
	cy_cb_params_t       zcallbacks[CY_ON_MAX];

	long                 timeout     ;
	struct timeval       tv          ;

	aeEventLoop         *loop        ;

	int                  keepalive   ;

	/* fd type, default TCP */
	CY_FD_TYPE           protocol    ;

	/* check if is end of file. */
	int                  eof         ;

	cy_fd_base_t        *base        ;

	/* hook == base on default, if handler is set */
	cy_fd_base_t        *hook        ;

} cy_fd_t;

typedef struct _cy_buf_token
{
        uint32_t         off   ;
        uint32_t         len   ;
        uint32_t         end   ;
        uint32_t         parsed;

        char             buf[0];
}
cy_buf_token_t;

typedef struct _cy_object_base_t
{
	zend_object        zo       ;
	void              *ptr      ;

	cy_cb_free_func_t *free_func;
}
cy_object_base_t; /* extends zend_object */


typedef struct _cy_fd_base
{
	zval              *zentry      ;
	zval              *zrequest    ;
	zval              *zresponse   ;

	void              *data        ;

	/* cf means cy_fd_t instance in this project */
	cy_fd_t           *cf          ;

	cy_cb_hook_func_t *recv_cb     ;
	cy_cb_hook_func_t *send_cb     ;

	cy_buf_token_t    *buf         ;
}
cy_fd_base_t;

enum CY_EVENT_TYPE
{
        CY_EVENT_READ     =       1 << 0,
        CY_EVENT_WRITE    =       1 << 1,
        CY_EVENT_EXCEPT   =       1 << 2
};

extern zend_class_entry    *cy_loop_class_entry        ;
extern zend_class_entry    *cy_fd_class_entry          ;
extern zend_class_entry    *cy_http_class_entry        ;

extern zend_module_entry    cye_module_entry           ;

#define REGISTER_CYE_CLASS_ENTRY(name, cye_entry, class_functions) { \
	zend_class_entry ce; \
	INIT_CLASS_ENTRY(ce, name, class_functions); \
	ce.create_object = cy_objects_new; \
	cye_entry = zend_register_internal_class(&ce TSRMLS_CC); \
} \

#define CY_BUFFER_LENGTH 8192

typedef enum _CY_Config_Type
{
        CY_TYPE_UINT8  = 0 ,
        CY_TYPE_UINT16  ,
        CY_TYPE_UINT32  ,
        CY_TYPE_UINT64  ,

        CY_TYPE_STRING  ,
        CY_TYPE_ARRAY   ,
        CY_TYPE_OBJECT  ,

        CY_TYPE_OBJKEY  ,

        CY_TYPE_COND8   ,
        CY_TYPE_COND16  ,
        CY_TYPE_COND32  ,
        CY_TYPE_COND64  ,

        CY_TYPE_TLV     ,

        CY_TYPE_CASE8   ,
        CY_TYPE_CASE16  ,
        CY_TYPE_CASE32  ,
        CY_TYPE_CASE64  ,
}
CY_Config_Type;

#define CY_COMMON_COND 0x7FFFFFFF

#define CR  '\r'
#define LF  '\n'

#endif
