#ifndef __CY_STR_H_
#define __CY_STR_H_

#include <stdint.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef void (*cy_w_int)(char **ptr, uint16_t v);
typedef unsigned long (*cy_r_int )(const char **ptr);

void cy_w_int8 (char **ptr, uint8_t  v);
void cy_w_int16(char **ptr, uint16_t v);
void cy_w_int32(char **ptr, uint32_t v);
void cy_w_int64(char **ptr, uint64_t v);

unsigned long cy_r_int8 (const char **ptr);
unsigned long cy_r_int16(const char **ptr);
unsigned long cy_r_int32(const char **ptr);
unsigned long cy_r_int64(const char **ptr);

const char* cy_str2bin(void *dst , const char* src, size_t len   );
const char* cy_bin2str(void *dst , const char* src, size_t len   );

const char* cy_bin2str_ex(void *dst , const char* src, size_t len);
const char* cy_str2bin_ex(void *dst , const char* src, size_t len);

void cy_dump_binary(const void* data, size_t len);

#define cy_w_buffer(ptr, val, len) memcpy(ptr, val, len); ptr += len
#define cy_r_buffer(ptr, val, len) memcpy(val, ptr, len); ptr += len

#ifdef __cplusplus
}
#endif

#endif

