#ifndef __CY_CALCULATOR_H__
#define __CY_CALCULATOR_H__

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif 

int cy_calculator(const char* str, size_t length);

#ifdef __cplusplus
}
#endif 

#endif
