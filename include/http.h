#ifndef _CY_HTTP_H_
#define _CY_HTTP_H_

#include "src/http_parser.h"
#include "src/multipart_parser.h"

int http_request_begin          (http_parser *parser);
int http_request_on_url         (http_parser *parser, const char *at, size_t length);
int http_request_on_status      (http_parser *parser, const char *at, size_t length);
int http_request_on_header_field(http_parser *parser, const char *at, size_t length);
int http_request_on_header_value(http_parser *parser, const char *at, size_t length);
int http_request_on_body        (http_parser *parser, const char *at, size_t length);

int http_request_on_headers_complete(http_parser *parser);
int http_request_on_message_complete(http_parser *parser);

int http_request_free_zval(http_parser *parser);
int http_request_init_zval(http_parser *parser);


int http_response_begin          (http_parser *parser);
int http_response_on_url         (http_parser *parser, const char *at, size_t length);
int http_response_on_status      (http_parser *parser, const char *at, size_t length);
int http_response_on_header_field(http_parser *parser, const char *at, size_t length);
int http_response_on_header_value(http_parser *parser, const char *at, size_t length);
int http_response_on_body        (http_parser *parser, const char *at, size_t length);

int http_response_on_headers_complete(http_parser *parser);
int http_response_on_message_complete(http_parser *parser);

int http_response_free_zval(http_parser *parser);
int http_response_init_zval(http_parser *parser);


const http_parser_settings*   cy_http_request_settings ();
const http_parser_settings*   cy_http_response_settings();

int http_request_body_parse_default    (void *p, const char *at, size_t length);
int http_request_body_parse_rfc1867    (void *p, const char *at, size_t length);

int cy_treat_data(int arg, const char *str, int len, zval* array_ptr, char *separator TSRMLS_DC);

typedef int (*cy_body_cb_func_t)(void *p, const char *at, size_t length);

#define CY_MAX_COOKIE_LEN 8192
#define CY_MAX_METHOD_LEN 34

typedef struct _cy_str_token {
	const char *str;
	uint32_t    len;
} cy_str_token_t;

enum cy_str_token_type_t
{
	CURRENT_HEAD_NAME           = 0,
	CURRENT_HEAD_VALUE          = 1,

	CURRENT_FORM_NAME           = 2,
	CURRENT_FORM_VALUE          = 3,

	CURRENT_BODY                = 4,
	CURRENT_URL                    ,
	CURRENT_TK_MAX
};

typedef struct _cy_http_zval_request
{
	zval *headers    ;
	zval *cookies    ;
	zval *get        ;
	zval *post       ;
	zval *request    ;
	zval *files      ;
	zval *servers    ;

	zval *disposition;

	/*  for multi file form, post data */
	zval *current_file_ptr;
}
cy_http_zval_request_t;

typedef struct _cy_http_zval_response
{
	zval *headers    ;
	zval *body       ;
}
cy_http_zval_response_t;

typedef struct cy_http_client
{
	int                  gzip     ;
	int                  keepalive;
	
	int                  eof      ;

	cy_http_zval_request_t  req   ;
	cy_http_zval_response_t rsp   ;

	cy_str_token_t       index[CURRENT_TK_MAX];

        cy_body_cb_func_t    body_cb  ;
        const char          *boundary ;
	cy_fd_base_t        *base     ;

	/* http_parser  */
	http_parser         *parser   ;

	const http_parser_settings *settings;
	void                *data     ;
}
cy_http_client_t;

#endif
