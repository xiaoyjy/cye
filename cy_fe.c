/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2012 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id: header 321634 2012-01-01 13:15:04Z felipe $ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"

#include "cy_fe.h"
#include "structs.h"
#include "src/hashtable.h"

#include "cye.h"

#include <SAPI.h>
#include <dlfcn.h>

#define CY_SHMKEY           "15161278"
#define CY_SHMSIZE          "32"

//最大hash深度为32
#define CY_ROW_NUM          "32"

#define CY_MAX_CTL_NUM      "5"
#define CY_MAX_CTL_TIME     "60"

ZEND_DECLARE_MODULE_GLOBALS(cye)

zend_class_entry   *cy_fd_class_entry           ;
zend_class_entry   *cy_loop_class_entry         ;
zend_class_entry   *cy_http_class_entry       ;
zend_class_entry   *cy_http_class_entry       ;

/* {{{ cye_module_entry
 */

/* True global resources - no need for thread safety here */
static int le_cye;

/* {{{ cye_functions[]
 *
 * Every user visible function must have an entry in cye_functions[].
 */
const zend_function_entry cye_functions[] = {

        PHP_FE(cy_i_init                 , NULL                         )
        PHP_FE(cy_i_drop                 , NULL                         )
        PHP_FE(cy_i_next                 , NULL                         )
        PHP_FE(cy_i_info                 , NULL                         )
        PHP_FE(cy_i_reset                , NULL                         )
        PHP_FE(cy_i_set                  , NULL                         )
        PHP_FE(cy_i_get                  , NULL                         )
        PHP_FE(cy_i_del                  , NULL                         )
        PHP_FE(cy_i_inc                  , NULL                         )
        PHP_FE(cy_i_dec                  , NULL                         )

        PHP_FE(cy_s_init                 , NULL                         )
        PHP_FE(cy_s_drop                 , NULL                         )
        PHP_FE(cy_s_next                 , NULL                         )
        PHP_FE(cy_s_info                 , NULL                         )
        PHP_FE(cy_s_reset                , NULL                         )
        PHP_FE(cy_s_set                  , NULL                         )
        PHP_FE(cy_s_get                  , NULL                         )
        PHP_FE(cy_s_del                  , NULL                         )

        PHP_FE(cy_create_lock            , NULL                         )
        PHP_FE(cy_lock                   , NULL                         )
        PHP_FE(cy_unlock                 , NULL                         )

        PHP_FE(cy_errno                  , NULL                         )
        PHP_FE(cy_title                  , NULL                         )

        PHP_FE(cy_ctl_check              , NULL                         )
        PHP_FE(cy_ctl_succ               , NULL                         )
        PHP_FE(cy_ctl_fail               , NULL                         )

        PHP_FE(cy_unpack                 , NULL                         )
        PHP_FE(cy_pack                   , NULL                         )

        PHP_FE(cy_split_by_tag           , NULL                         )
        PHP_FE(cy_http_parse             , NULL                         )

        PHP_FE(cy_ae_add                 , NULL                         )
        PHP_FE(cy_ae_del                 , NULL                         )
        PHP_FE(cy_ae_pause               , NULL                         )
        PHP_FE(cy_ae_resume              , NULL                         )
        PHP_FE(cy_ae_run                 , NULL                         )
        PHP_FE(cy_ae_stop                , NULL                         )
        PHP_FE(cy_ae_set_interval        , NULL                         )
        PHP_FE(cy_ae_del_interval        , NULL                         )

	PHP_FE(cy_fd_set_opt             , NULL                         )
	PHP_FE(cy_fd_on                  , NULL                         )
	PHP_FE(cy_fd_curl_multi          , NULL                         )
        PHP_FE(cy_fd_set_handler         , NULL                         )

        PHP_FE_END      /* Must be the last line in cye_functions[] */
};
/* }}} */

const zend_function_entry cy_loop_methods[] = {
	PHP_FALIAS(__construct , cy_ae_loop_construct , NULL                  )
	PHP_FALIAS(add         , cy_ae_add            , NULL                  )
	PHP_FALIAS(del         , cy_ae_del            , NULL                  )
	PHP_FALIAS(pause       , cy_ae_pause          , NULL                  )
	PHP_FALIAS(resume      , cy_ae_resume         , NULL                  )
	PHP_FALIAS(run         , cy_ae_run            , NULL                  )
	PHP_FALIAS(stop        , cy_ae_stop           , NULL                  )
	PHP_FALIAS(set_interval, cy_ae_set_interval   , NULL                  )
	PHP_FALIAS(del_interval, cy_ae_del_interval   , NULL                  )
        PHP_FE_END
};

const zend_function_entry cy_fd_methods[] = {
	PHP_FALIAS(__construct , cy_fd_construct      , NULL                  )
	PHP_FALIAS(on          , cy_fd_on             , NULL                  )
	PHP_FALIAS(set_opt     , cy_fd_set_opt        , NULL                  )
	PHP_FALIAS(set_handler , cy_fd_set_handler    , NULL                  )
        PHP_FE_END
};

const zend_function_entry cy_http_methods[] = {
	PHP_FALIAS(__construct , cy_http_construct   , NULL                  )
	PHP_FALIAS(parse       , cy_http_parse       , NULL                  )
        PHP_FE_END
};

zend_module_entry cye_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
        STANDARD_MODULE_HEADER,
#endif
        "cye",
        cye_functions,
        PHP_MINIT(cye),
        PHP_MSHUTDOWN(cye),
        NULL,
        NULL,
        PHP_MINFO(cye),
#if ZEND_MODULE_API_NO >= 20010901
        "0.1", /* Replace with version number for your extension */
#endif
        STANDARD_MODULE_PROPERTIES
};
/* }}} */

BEGIN_EXTERN_C()
#ifdef COMPILE_DL_CYE
ZEND_GET_MODULE(cye)
#endif
END_EXTERN_C()

/* {{{ PHP_INI
 */
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("cye.shm_key"  ,  CY_SHMKEY       , PHP_INI_ALL, OnUpdateLong, shm_key  , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.shm_size" ,  CY_SHMSIZE      , PHP_INI_ALL, OnUpdateLong, shm_size , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.deep"     ,  CY_ROW_NUM      , PHP_INI_ALL, OnUpdateLong, deep     , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.cycle"    ,  CY_MAX_CTL_TIME , PHP_INI_ALL, OnUpdateLong, cycle    , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.max_fail" ,  CY_MAX_CTL_NUM  , PHP_INI_ALL, OnUpdateLong, max_fail , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.enable"   ,  "1"             , PHP_INI_ALL, OnUpdateLong, enable   , zend_cye_globals, cye_globals)
PHP_INI_END()
/* }}} */

/* {{{ php_cye_init_globals
 */
static void php_cye_init_globals(zend_cye_globals *cye_globals)
{

}
/* }}} */

static void cy_free_storage(void *object TSRMLS_DC)
{
	cy_object_base_t   *intern = (cy_object_base_t *)object;
	zend_object_std_dtor(&intern->zo TSRMLS_CC);
	if(intern->free_func)
	{
		(*intern->free_func)(intern->ptr);
	}

	efree(intern);
}

zend_object_value cy_objects_new(zend_class_entry *class_type TSRMLS_DC)
{
	zend_object_value    retval;
	cy_object_base_t          *intern;

	intern = emalloc (sizeof(cy_object_base_t));
        memset(intern, 0, sizeof(cy_object_base_t));

	zend_object_std_init  (&intern->zo, class_type TSRMLS_CC);
	object_properties_init(&intern->zo, class_type);

	retval.handle = zend_objects_store_put(intern,
			(zend_objects_store_dtor_t) zend_objects_destroy_object,
			cy_free_storage, NULL TSRMLS_CC);

	retval.handlers = zend_get_std_object_handlers();
	return retval;
}

#define CONSTANT_TYPE_MAP \
	XX(SYS        ) XX(CTL        ) XX(CFG_I      ) XX(LOCK       ) XX(CFG_B      )  \
	XX(UINT8      ) XX(UINT16     ) XX(UINT32     ) XX(UINT64     ) XX(STRING     )  \
	XX(ARRAY      ) XX(OBJECT     ) XX(OBJKEY     )                                  \
	XX(COND8      ) XX(COND16     ) XX(COND32     ) XX(COND64     ) XX(TLV        )  \
	XX(CASE8      ) XX(CASE16     ) XX(CASE32     ) XX(CASE64     )

#define CONSTANT_ON_MAP \
	XY(REQUEST    ) XY(RESPONSE   ) XY(IDLE       ) XY(CONNECT    ) XY(ACCEPT     )  \
	XY(CLOSE      ) XY(TIMEOUT    ) XY(ERROR      ) XY(END        ) XY(READY      )  \
	XY(MAX        ) 

#define CONSTANT_YY_MAP \
	YY(COMMON_COND)

#define SHM_INT_INIT_MAP \
	XX(SYS        ) XX(CTL        ) XX(CFG_I      )

#define SHM_BIN_INIT_MAP \
	XX(LOCK       ) XX(CFG_B      ) 

/* {{{ PHP_MINIT_FUNCTION
*/
PHP_MINIT_FUNCTION(cye)
{
        REGISTER_INI_ENTRIES();

#define XX(type) cy_i_init(CY_TYPE_##type);
	SHM_INT_INIT_MAP
#undef  XX

#define XX(type) cy_b_init(CY_TYPE_##type);
	SHM_BIN_INIT_MAP
#undef  XX

#define YY(type) REGISTER_LONG_CONSTANT("CY_"#type, CY_##type, CONST_CS | CONST_PERSISTENT);
#define XX(type) YY(TYPE_##type)
	CONSTANT_TYPE_MAP
#undef  XX
#define XY(type) YY(ON_##type  )
	CONSTANT_ON_MAP
#undef  XY
	CONSTANT_YY_MAP
#undef  YY

#ifndef PHP_SYSTEM_PROVIDES_SETPROCTITLE
        sapi_module_struct *symbol;
        symbol = &sapi_module;
        if(symbol)
        {
                _mc_G(proc_title) = symbol->executable_location;
        }
#endif

	REGISTER_CYE_CLASS_ENTRY("cy_loop"     , cy_loop_class_entry     , cy_loop_methods     );
	REGISTER_CYE_CLASS_ENTRY("cy_fd"       , cy_fd_class_entry       , cy_fd_methods       );
	REGISTER_CYE_CLASS_ENTRY("cy_http"     , cy_http_class_entry     , cy_http_methods     );
        return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
*/
PHP_MSHUTDOWN_FUNCTION(cye)
{
        UNREGISTER_INI_ENTRIES();
        return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
*/
PHP_MINFO_FUNCTION(cye)
{
        php_info_print_table_start();
        php_info_print_table_header(2, "cye support", "enabled");
        php_info_print_table_header(2, "By", "xiaoyjy@gmail.com");
        php_info_print_table_end();

        DISPLAY_INI_ENTRIES();
}
/* }}} */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
