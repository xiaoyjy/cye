#ifdef HAVE_CONFIG_H

#include "config.h"
#endif

#include <php.h>

#include "cy_fe.h"
#include "structs.h"

#include "ae/ae.h"
#include "ae/ae_cb.h"

#include <sys/socket.h>

/* client handler table */
#define CY_CLIENT_DO_TABLE(t) \
	/*CY_EVENT_STEP_IDLE     , CY_EVENT_STEP_CONNECT    , CY_EVENT_STEP_ACCEPT      */ \
	{ NULL                   , cy_##t##_do_connect      , NULL                      ,  \
	/*CY_EVENT_STEP_CLOSE    , CY_EVENT_STEP_READABLE   , CY_EVENT_STEP_WRITEABLE   */ \
	  cy_##t##_do_close      , cy_##t##_do_cli_readable , cy_##t##_do_cli_writeable ,  \
	/*CY_EVENT_STEP_TIMEOUT  , CY_EVENT_STEP_END                                    */ \
	  cy_##t##_do_timeout    , cy_##t##_do_end                                      }

/* server handler table */
#define CY_SERVER_DO_TABLE(t) \
	/*CY_EVENT_STEP_IDLE     , CY_EVENT_STEP_CONNECT    , CY_EVENT_STEP_ACCEPT      */ \
	{ NULL                   , NULL                     , cy_##t##_do_accept        ,  \
	/*CY_EVENT_STEP_CLOSE    , CY_EVENT_STEP_READABLE   , CY_EVENT_STEP_WRITEABLE   */ \
	  cy_##t##_do_close      , cy_##t##_do_srv_readable , cy_##t##_do_srv_writeable ,  \
	/*CY_EVENT_STEP_TIMEOUT  , CY_EVENT_STEP_END                                    */ \
	  cy_##t##_do_timeout    , cy_##t##_do_end                                      }

static cy_cb_hook_func_t* cb_do_table[][CY_EVENT_STEP_MAX] = { CY_CLIENT_DO_TABLE(cb), CY_SERVER_DO_TABLE(cb) };
static cy_cb_hook_func_t* ce_do_table[][CY_EVENT_STEP_MAX] = { CY_CLIENT_DO_TABLE(ce), CY_SERVER_DO_TABLE(ce) };

typedef struct cy_ae_data
{
	cy_fd_t           *cf  ;
	struct cy_ae_data *next;
}
cy_ae_data_t;

typedef struct cy_ae_loop
{
	aeEventLoop     *loop    ;
	int              size    ;
	cy_ae_data_t    *head    ;
	cy_ae_data_t    *data    ;

	long             timeval ;
	long long        timeid  ;
	zval            *timecb  ;
}
cy_ae_loop_t;

int cy_ae_do_timer(aeEventLoop *loop, long long id, void *data)
{
	cy_ae_loop_t        *lp     = data    ;
	cy_fd_t             *cf               ;
	struct timeval       now              ;
	int                  ret              ;

	cy_ae_data_t        *p      = lp->head; 
	cy_ae_data_t        *prev   = NULL    ;

	gettimeofday(&now, NULL);
	//DEBUG_PRINT("at %s:%d %s %lld %u\n", __FILE__,  __LINE__, __FUNCTION__, id, getpid());

#if 0
	cy_ae_data_t *n = lp->head;
	printf("start %x node size %d, count %d, end %x\n",
		lp->data, sizeof(cy_ae_data_t), lp->size, lp->data + sizeof(cy_ae_data_t)*lp->size);
	while(n)
	{
		printf("n %x - %x %ld, refcount %d\n", n, n->cf, n->cf->timeout, n->cf->zentry->refcount__gc);
		n = n->next;
	}

#endif
	zval   *retval;
	ret = call_user_function_ex(CG(function_table), NULL, lp->timecb, &retval, 0, NULL, 0, NULL TSRMLS_CC);

#define cy_cost_time(now, old) ((now.tv_sec-old.tv_sec)*1000000+now.tv_usec-old.tv_usec)
	while(p != NULL)
	{
		cf = p->cf;
		if(cf && cy_cost_time(now, cf->tv) > cf->timeout)
		{
			cf->step  = CY_EVENT_STEP_TIMEOUT                                   ;
			cf->state = CY_ON_TIMEOUT                                           ;
			cy_ae_do_event(lp->loop, cf->fd, cf, CY_EVENT_READ | CY_EVENT_WRITE);			
			p->cf     = NULL;
		}

		if(p->cf == NULL)
		{
			if(prev == NULL)
			{
				lp->head   = p->next;
			}
			else
			{
				prev->next = p->next; 
			}
		}
		else
		{
			prev = p;
		}

		p = p->next;
	}

	return lp->timeval;
}

/**
 * void cy_ae_do_event(aeEventLoop *loop, int fd, void *privdata, int mask)
 */
void cy_ae_do_event(aeEventLoop *loop, int fd, void *privdata, int mask)
{
	cy_cb_hook_func_t   *event_cb                                     ;
	cy_fd_t             *cf           = privdata                      ;
	int                  step         = 1                             ;
	cy_cb_params_t      *fcb                                          ;
	int                  type         = (int)(cf->type>0)             ;

	/* reset recv event type */
	cf->mask = mask;

	DEBUG_PRINT_FUNC_START
	DEBUG_PRINT("cf->type %d , cf->step %d, cf->state %d\n", cf->type, cf->step, cf->state);

	/* do something before call_user_function(PHP callback function) */
	if((event_cb = cb_do_table[type][cf->step]) != NULL)
	{
		cf->result = (*event_cb)(cf);
		if(cf->result < 0)
		{
			goto error;
		}
	}

	step++;

	/* execute PHP callback function, error(result < 0); call on order(result == 0); call and break(result > 0) */
	if(cf->zcallbacks[cf->state].cb)
	{
		cf->result = cy_ae_call_user_function_ex(cf, &cf->zcallbacks[cf->state]);
		if     (cf->result < 0)
			goto error;
		else if(cf->result > 0)
			return;
	}

	step++;

	/* internal callback function, disable if use callback function is set. */
	if(!cf->zcallbacks[cf->state].cb && cf->ccallbacks[cf->state].func)
	{
		cf->result = (*cf->ccallbacks[cf->state].func)(&cf->ccallbacks[cf->state]);
		if     (cf->result < 0)
			goto error;
		else if(cf->result > 0)
			return;
	}

	step++;

	/* do something after call_user_function(PHP callback function) */
	if((event_cb = ce_do_table[type][cf->step]) != NULL)
	{
		cf->result = (*event_cb)(cf);
		if(cf->result < 0)
		{
			goto error;
		}
	}

	return;

error:
	fcb = &cf->zcallbacks[CY_ON_ERROR];
	if(fcb->cb)
	{
		if(fcb->data)
		{
			zval_ptr_dtor(&fcb->data);
		}

		MAKE_STD_ZVAL           (fcb->data                                             );
		array_init              (fcb->data                                             );
		add_assoc_long          (fcb->data , "errno"            , errno                );
		add_assoc_string        (fcb->data , "error"            , strerror(errno), 1   );
		add_assoc_long          (fcb->data , "step"             , cf->step             );
		add_assoc_long          (fcb->data , "state"            , cf->state            );
		add_assoc_long          (fcb->data , "retval"           , cf->result           );
		add_assoc_long          (fcb->data , "call_step"        , step                 );
		cy_ae_call_user_function(cf        , CY_ON_ERROR                               );

		/* free it after user function called. */
		zval_ptr_dtor(&fcb->data); fcb->data = NULL;
	}
	else
	{
		zend_error(E_WARNING,
				"ae_do_event errno=%d %s, state=%d, result=%d, call=%d.\n "
				"define CY_ON_ERROR handler disable this warning, eg:\n " 
				"\t$fd->on(CY_ON_ERROR, $callback); ",
				errno, strerror(errno), cf->state, cf->result, step);
	}

	if(cf->state != CY_EVENT_STEP_END)
	{
		/* call end functions */
		cy_cb_do_end            	(cf                 );
		cy_ae_call_user_function	(cf, CY_ON_END      );
		cy_ce_do_end            	(cf                 );
	}

}

void cy_ae_loop_free(void *p)
{
	DEBUG_PRINT_FUNC_START

		cy_ae_loop_t *lp = p;
	efree(lp->data);
	aeDeleteEventLoop(lp->loop);
	efree(lp);
}

PHP_FUNCTION(cy_ae_loop_construct)
{
	DEBUG_PRINT_FUNC_START

		zval    *object                   ;
	long     size                     ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"Ol", &object, cy_loop_class_entry, &size) == FAILURE)
	{
		return;
	}

	int              data_size = sizeof(cy_ae_data_t) * size       ;

	cy_ae_loop_t    *lp        = emalloc(sizeof(cy_ae_loop_t)     );
	lp->data                   = emalloc(data_size                );
	lp->head                   = NULL                              ;
	memset(lp->data, 0         , data_size                        );

	lp->timeval                = 100                               ;
	lp->timeid                 = 0                                 ;
	lp->size                   = size                              ;
	lp->loop                   = aeCreateEventLoop(size)           ;
	cy_object_base_t      *o         = zend_object_store_get_object(object TSRMLS_CC);
	o->ptr                     = lp                                ;
	o->free_func               = cy_ae_loop_free                   ;
}

PHP_FUNCTION(cy_ae_add)
{
	DEBUG_PRINT_FUNC_START

	zval *object;
	zval *zentry;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"OO", &object, cy_loop_class_entry, &zentry, cy_fd_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t   *lp     = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	cy_fd_t        *cf     = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr;
	if(cf->fd < 1 || 
			aeCreateFileEvent(lp->loop,
				cf->fd, cf->mask, cy_ae_do_event, cf) == AE_ERR)
	{
		RETURN_BOOL(0);
	}

	gettimeofday(&cf->tv, NULL);

	cf->loop                        = lp->loop           ;
	lp->data[cf->fd].cf             = cf                 ;
	if(NULL == lp->data[cf->fd].next)
	{
		lp->data[cf->fd].next   = lp->head           ;
		lp->head                = lp->data + cf->fd  ;	
	}

	Z_ADDREF_P(cf->zentry);
	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_ae_del)
{
	DEBUG_PRINT_FUNC_START

	zval *object;
	zval *zentry;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"OO", &object, cy_loop_class_entry, &zentry, cy_fd_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t   *lp     = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	cy_fd_t        *cf     = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr;
	lp->data[cf->fd].cf    = NULL;

	zval_ptr_dtor(&cf->zentry);
	aeDeleteFileEvent(lp->loop, cf->fd, cf->mask);
	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_ae_pause)
{
	DEBUG_PRINT_FUNC_START

	zval *object;
	zval *zentry;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"OO", &object, cy_loop_class_entry, &zentry, cy_fd_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t   *lp     = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	cy_fd_t        *cf     = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr;
	aeDeleteFileEvent(lp->loop, cf->fd, cf->mask);
	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_ae_resume)
{
	DEBUG_PRINT_FUNC_START

	zval *object;
	zval *zentry;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"OO", &object, cy_loop_class_entry, &zentry, cy_fd_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t   *lp     = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	cy_fd_t        *cf     = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr;
	if(aeCreateFileEvent(lp->loop, cf->fd, cf->mask, cy_ae_do_event, cf) == AE_ERR)
	{
		RETURN_BOOL(0);
	}

	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_ae_set_interval)
{
	long long   id       ;
	long        timeval  ;
	zval       *object   ;
	zval       *callback ;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"Olz", &object, cy_loop_class_entry, &timeval, &callback) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t *lp = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	if(lp->timeid)
	{
		zend_error(E_WARNING, "interval already seted.");
		RETURN_BOOL(0);
	}

	if((id = aeCreateTimeEvent(lp->loop, timeval, cy_ae_do_timer, lp, NULL)) == AE_ERR)
	{
		zend_error(E_ERROR, "Can't create the server cron time event.");
		RETURN_BOOL(0);
	}

	lp->timeid       = id;
	lp->timeval      = timeval ;
	lp->timecb       = callback;
	Z_ADDREF_P(lp->timecb);
	RETURN_BOOL(1);
}


PHP_FUNCTION(cy_ae_del_interval)
{
	zval       *object   ;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"O", &object, cy_loop_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t *lp = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	if(!lp->timeid)
	{
		RETURN_BOOL(0);
	}

	aeDeleteTimeEvent(lp->loop, lp->timeid);

	lp->timeid = 0   ;
	lp->timecb = NULL;

	zval_ptr_dtor(&lp->timecb);
	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_ae_run)
{
	zval *object;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"O", &object, cy_loop_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t *lp = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	aeMain(lp->loop);

	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_ae_stop)
{
	zval *object;
	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"O", &object, cy_loop_class_entry) == FAILURE) {
		RETURN_BOOL(0);
	}

	cy_ae_loop_t *lp = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr;
	aeStop(lp->loop);
	RETURN_BOOL(1);
}

