#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"

#include "cy_fe.h"
#include "src/hashtable.h"

#include <pthread.h>
#include <errno.h>

#ifndef PHP_SYSTEM_PROVIDES_SETPROCTITLE

#define MAX_TITLE_LENGTH 128

static void setproctitle(const char *title, int tlen)
{
        /* title too long => truncate */
	int len = strlen(_mc_G(proc_title));
        if (tlen >= len)
        {
                tlen = len;
        }

        memset(_mc_G(proc_title), 0x20, len);
        memcpy(_mc_G(proc_title), title, tlen);
} 

#endif 

PHP_FUNCTION(cy_i_inc)
{
        const char *key;
        int keylen;

        long val, type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sll", &key, &keylen, &type, &val) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_i_inc(key, keylen, type, val));
}

PHP_FUNCTION(cy_i_dec)
{
        const char *key;
        int keylen;

        long val, type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sll", &key, &keylen, &type, &val) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_i_dec(key, keylen, type, val));
}

PHP_FUNCTION(cy_i_init)
{
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_init error parameters.");
                RETURN_BOOL(0);
        }

        cy_i_init(type);
}

PHP_FUNCTION(cy_i_drop)
{
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_drop error parameters.");
                RETURN_BOOL(0);
        }

        cy_i_drop(type);
}

PHP_FUNCTION(cy_i_reset)
{
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_reset error parameters.");
                RETURN_BOOL(0);
        }

        cy_i_reset(type);
}

PHP_FUNCTION(cy_i_next)
{
        char key[48];
        uint32_t val;
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_next error parameters.");
                RETURN_BOOL(0);
        }

        int r = cy_i_next(key, type, &val);
        if(!r)
        {
                RETURN_FALSE;
        }

        array_init(return_value);
        add_assoc_long_ex(return_value, key, strlen(key) + 1, val);
}

PHP_FUNCTION(cy_i_set)
{
        const char *key;
        int keylen;
        long val, type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sll", &key, &keylen, &type, &val) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_i_set(key, keylen, type, val));
}

PHP_FUNCTION(cy_i_get)
{
        const char *key;
        int keylen;
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &type) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_LONG(cy_i_get(key, keylen, type));
}

PHP_FUNCTION(cy_i_del)
{
        const char *key;
        int keylen;
        long type;

        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &type) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_i_del(key, keylen, type));
}

PHP_FUNCTION(cy_i_info)
{
        char info[1024];
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_next error parameters.");
                RETURN_BOOL(0);
        }

        cy_i_info(type, info, 1024);
        RETURN_STRINGL(info, strlen(info) + 1, 1);
}

PHP_FUNCTION(cy_s_init)
{
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_init error parameters.");
                RETURN_BOOL(0);
        }

        cy_b_init(type);
}

PHP_FUNCTION(cy_s_drop)
{
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_drop error parameters.");
                RETURN_BOOL(0);
        }

        cy_b_drop(type);
}

PHP_FUNCTION(cy_s_reset)
{
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_reset error parameters.");
                RETURN_BOOL(0);
        }

        cy_b_reset(type);
}

PHP_FUNCTION(cy_s_next)
{
        char key[48];
        lv val;
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_i_next error parameters.");
                RETURN_BOOL(0);
        }

        if(!cy_b_next(key, type, &val))
        {
                RETURN_FALSE;
        }

        array_init(return_value);
        add_assoc_stringl_ex(return_value, key, strlen(key) + 1, val.v, val.l, 1);
}

PHP_FUNCTION(cy_s_set)
{
        const char *key, *val;
        int keylen, vallen;
        long type;

        lv lv_val;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sls", &key, &keylen, &type, &val, &vallen) == FAILURE)
        {
                RETURN_FALSE;
        }

        if(vallen > CY_HS_VAL_LEN - 1)
        {
                vallen = CY_HS_VAL_LEN;
        }

        lv_val.l = vallen;
        memcpy(lv_val.v, val, vallen);
        lv_val.v[vallen] = 0;
        RETURN_BOOL(cy_b_set(key, keylen, type, &lv_val));
}

PHP_FUNCTION(cy_s_get)
{
        const char *key;
        int keylen;
        long type;

        lv *vp;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &type) == FAILURE)
        {
                RETURN_STRING("", 1);
        }

        vp = cy_b_get(key, keylen, type);
        if(vp == NULL)
        {
                RETURN_STRING("", 1);
        }

        RETURN_STRINGL(vp->v, vp->l, 1);
}

PHP_FUNCTION(cy_s_del)
{
        const char *key;
        int keylen;
        long type;

        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &type) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_b_del(key, keylen, type));
}

PHP_FUNCTION(cy_s_info)
{
        char info[1024];
        long type;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"l", &type) == FAILURE)
        {
                zend_error(E_WARNING, "cy_s_info error parameters.");
                RETURN_BOOL(0);
        }

        cy_b_info(type, info, 1024);
        RETURN_STRINGL(info, strlen(info) + 1, 1);
}

PHP_FUNCTION(cy_create_lock)
{
        pthread_mutex_t *lock;
        pthread_mutexattr_t attr;
        lv val, *vp;

        const char* key;
        int keylen;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"s", &key, &keylen) == FAILURE)
        {
                zend_error(E_WARNING, "cy_create_lock error parameters.");
                RETURN_FALSE;
        }

        memset(&val, 0, sizeof(lv));
        if(!cy_b_set(key, keylen, CY_TYPE_LOCK, &val))
        {
                RETURN_FALSE;
        }

        vp = cy_b_get(key, keylen, CY_TYPE_LOCK);
        if(vp == NULL)
        {
                RETURN_FALSE;
        }
        
        // 极为坑爹的bug, http://blog.csdn.net/yangdongqian/article/details/8286962
        vp->l = sizeof(pthread_mutex_t) + (long)vp->v%sizeof(void*);
        //lock = (pthread_mutex_t*)(&vp->v + vp->l - sizeof(pthread_mutex_t));
        lock = (pthread_mutex_t*)(&vp->v + (long)vp->v%sizeof(void*));
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
        pthread_mutex_init(lock, &attr);
        RETURN_TRUE;
}

PHP_FUNCTION(cy_lock)
{
        const char* key;
        int keylen;
        lv *vp;
        pthread_mutex_t *lock;

        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"s", &key, &keylen) == FAILURE)
        {
                zend_error(E_WARNING, "cy_lock error parameters.");
                RETURN_FALSE;
        }

        vp = cy_b_get(key, keylen, CY_TYPE_LOCK);
        if(vp == NULL)
        {
                RETURN_FALSE;
        }
        
        // 极为坑爹的bug, http://blog.csdn.net/yangdongqian/article/details/8286962
        //lock = (pthread_mutex_t*)(&vp->v + vp->l - sizeof(pthread_mutex_t));
        lock = (pthread_mutex_t*)(&vp->v + (long)vp->v%sizeof(void*));
        RETURN_BOOL(pthread_mutex_lock(lock) == 0);
}

PHP_FUNCTION(cy_unlock)
{
        const char* key;
        int keylen;
        lv *vp;
        pthread_mutex_t *lock;

        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"s", &key, &keylen) == FAILURE)
        {
                zend_error(E_WARNING, "cy_unlock error parameters.");
                RETURN_FALSE;
        }

        vp = cy_b_get(key, keylen, CY_TYPE_LOCK);
        if(vp == NULL)
        {
                RETURN_FALSE;
        }
        
        // 极为坑爹的bug, http://blog.csdn.net/yangdongqian/article/details/8286962
        //lock = (pthread_mutex_t*)(&vp->v + vp->l - sizeof(pthread_mutex_t));
        lock = (pthread_mutex_t*)(&vp->v + (long)vp->v%sizeof(void*));
        RETURN_BOOL(pthread_mutex_unlock(lock) == 0);
}

PHP_FUNCTION(cy_ctl_check)
{
        const char *key;
        int keylen;

        long now;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &now) == FAILURE)
        {
                zend_error(E_WARNING, "cy_ctl_check error parameters.");
                RETURN_TRUE;
        }

        RETURN_BOOL(cy_ctl_check(key, keylen, now));
}

PHP_FUNCTION(cy_ctl_succ)
{
        const char *key;
        int keylen;

        long now;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &now) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_ctl_succ(key, keylen, now));
}

PHP_FUNCTION(cy_ctl_fail)
{
        const char *key;
        int keylen;

        long now;
        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"sl", &key, &keylen, &now) == FAILURE)
        {
                RETURN_FALSE;
        }

        RETURN_BOOL(cy_ctl_fail(key, keylen, now));
}

PHP_FUNCTION(cy_errno)
{
	long new_errno = 0;
	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"|l", &new_errno) == SUCCESS)
	{
		if(!new_errno)
		{
			errno = new_errno;
		}
	}

        RETURN_LONG(errno);
}

PHP_FUNCTION(cy_title)
{
        const char *title;
        int tlen;

        if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, (char*)"s", &title, &tlen) == FAILURE)
        {
                RETURN_BOOL(0);
        }

#ifndef PHP_SYSTEM_PROVIDES_SETPROCTITLE
        /* local (incompatible) setproctitle */
        setproctitle(title, tlen);
#else
        /* let's use system setproctitle() (BSD or compatible) */
        setproctitle("%s%c", title, 0);
#endif
}

