#include <php.h>

#include "cy_fe.h"
#include "structs.h"
#include "ae/ae_cb.h"

cy_fd_t *cy_fd_init(void *zentry, int fd, int type, long timeout)
{
	DEBUG_PRINT_FUNC_START

	cy_fd_base_t *base                         ;
	cy_cb_params_t      *fcb                          ;
	int                  i                            ;
	cy_fd_t             *cf = emalloc(sizeof(cy_fd_t));

	memset(cf       , 0        , sizeof(cy_fd_t));
	cf->zentry  = zentry           ;
	cf->fd      = fd               ;
	cf->type    = type             ;
	cf->timeout = timeout          ;
	for(i = 0; i < CY_ON_MAX; i++)
	{
		cf->ccallbacks[i].cf = cf;
		cf->zcallbacks[i].cf = cf;
	}

	/* instance cy_net_param_t   */
	base = emalloc( sizeof(cy_fd_base_t));
	memset(base, 0, sizeof(cy_fd_base_t));
	base->recv_cb         = cy_fd_internal_recv ;
	base->send_cb         = NULL                ;
	base->buf             = emalloc (CY_BUF_INITIA_LEN + sizeof(cy_buf_token_t) + sizeof(size_t));
	memset(base->buf, 0, sizeof(cy_buf_token_t));
	base->buf->len        = CY_BUF_INITIA_LEN   ;

	cf  ->hook            = base                ;
	cf  ->base            = base                ;
	switch(type)
	{
	case 1:
		/* server */
		cf->state   = CY_ON_REQUEST         ;
		cf->step    = CY_EVENT_STEP_READABLE;
		cf->mask    = CY_EVENT_READ         ;
		
		fcb         = &cf->ccallbacks[CY_ON_REQUEST];
		cy_fd_set_ccallback(fcb, base->recv_cb, NULL, NULL);
		break;

	case 2:
		/* accept fd */
		cf->state   = CY_ON_ACCEPT          ;
		cf->step    = CY_EVENT_STEP_ACCEPT  ;
		cf->mask    = CY_EVENT_READ         ;
		break;

	default:
		/* client */
		cf->state   = CY_ON_CONNECT         ;
		cf->step    = CY_EVENT_STEP_CONNECT ;
		cf->mask    = CY_EVENT_WRITE        ;
		fcb         = &cf->ccallbacks[CY_ON_RESPONSE];
		cy_fd_set_ccallback(fcb, base->recv_cb, NULL, NULL);
		break;
	}

	return cf;
}

void cy_fd_free(void* ptr)
{
	DEBUG_PRINT_FUNC_START
	cy_fd_t *cf = ptr;
	int      i       ;

#define cy_fd_free_zval_ptr_dtor(type, member) if(cf->type[i].member) { \
		zval_ptr_dtor(&cf->type[i].member);                     \
		cf->type[i].member = NULL;                              \
	}

	for(i = 0; i < CY_ON_MAX; i++)
	{
		/* free all zend elements. */
		cy_fd_free_zval_ptr_dtor(zcallbacks, cb      );
		cy_fd_free_zval_ptr_dtor(zcallbacks, args    );
		cy_fd_free_zval_ptr_dtor(zcallbacks, data    );

		cy_fd_free_zval_ptr_dtor(ccallbacks, root    );
		cy_fd_free_zval_ptr_dtor(ccallbacks, cb      );
	}

	efree(cf->base->buf);
	efree(cf->base     );
	efree(cf           );
}

/*
cy_cb_params_t *cy_fd_get_internal(cy_fd_t *cf, CY_EVENT_STATE type)
{
	return &cf->ccallbacks[type];
}
*/

int cy_fd_set_ccallback (cy_cb_params_t *fcb, cy_cb_hook_func_t* func, zval *cb, zval *root)
{
	if(fcb->cb    ) 
		zval_ptr_dtor(&fcb->cb  );

	if(fcb->root  )
		zval_ptr_dtor(&fcb->root);

	fcb->func          = func        ;
	fcb->cb            = cb          ;
	fcb->root          = root        ;

	if(cb   ) Z_ADDREF_P(fcb->cb    );
	if(root ) Z_ADDREF_P(fcb->root  );
	return 0;
}

int cy_fd_set_zcallback (cy_cb_params_t *fcb, zval *cb, zval *args)
{
	if(fcb->cb    ) 
		zval_ptr_dtor(&fcb->cb  );

	if(fcb->args  ) 
		zval_ptr_dtor(&fcb->args);
	
	fcb->cb       = cb               ;
	Z_ADDREF_P(fcb->cb              );

	fcb->args     = args             ;
	if(args)
		Z_ADDREF_P(fcb->args);

	return 0;
}

int cy_fd_call_on_ready(cy_fd_t *cf)
{
	cy_fd_base_t         *base       = cf   ->hook          ;
	zval                 *data                              ;
	cy_cb_params_t       *fcb                               ;

	fcb = &cf->zcallbacks[CY_ON_READY];
	if(fcb->data)
		zval_ptr_dtor(&fcb->data) ;

	MAKE_STD_ZVAL(data);
	ZVAL_STRINGL (data, base->buf->buf, base->buf->off, 1)  ;

	fcb->data   =  data                                     ;
	cy_ae_call_user_function_ex(cf, fcb)                    ;

	return 0;
}

int cy_fd_internal_recv(void* param)
{
	/* in this function cf => cy_fd_t, c => client */
	cy_cb_params_t       *p          = param           ;
	cy_fd_t              *cf         = p    ->cf       ;
	cy_fd_base_t         *base       = cf   ->hook     ;
	cy_buf_token_t       *b          = base ->buf      ;
	int                   readed                       ;
	int                   available  = b->len - b->off ;

	/* realloc if space is not enough */
	if(available == 0)
	{
		if(b->len > CY_BUF_TOKEN_MAX_SIZE-1 /* set max buffer size 16M */)
		{
			zend_error(E_WARNING, "package over limits [16M]");
			b->off =  0;
			return -100;
		}

		cy_buf_token_t *new_ptr  = erealloc(b, sizeof(cy_buf_token_t) + (b->len << 1) + sizeof(size_t));
		if(!new_ptr)
		{
			zend_error(E_ERROR, "cy_fd_internal_recv: no memory");
			return -1;
		}

		available    = new_ptr->len         ;
		new_ptr->len = new_ptr->len << 1    ;
		base->buf    = b = p->ptr = new_ptr ;
	}

	/* must non_block. */
	DEBUG_PRINT("%d available before read\n", available);
	readed = read(cf->fd, b->buf + b->off, available);
	DEBUG_PRINT("%d readed\t, %d available\t, buf.len=%d\tbuf.used=%d\n", readed, available, b->len, b->off);
	if(readed < 0)
	{
		// EAGAIN must not happened here.
		return readed;
	}

	if(readed == 0)
	{
		/* readed == 0 eof or connection is broken. */
		if(cf->eof == 0)
		{
			/* eof file received successfully */
			cf->eof    = 1                  ;

			cy_fd_call_on_ready(cf);
		}
		else
		{
			/* eof again, connection is broken */
			cf->eof    = 0                  ;

			/* connection is borken. */
			cf->step  = CY_EVENT_STEP_CLOSE ;
			cf->state = CY_ON_CLOSE         ;

			/* status changed, ce_srv_readable never be called. */
			cy_ae_do_event(cf->loop, cf->fd, cf, CY_EVENT_READ|CY_EVENT_WRITE);

			/* set step to idle, avoid uncertainty jump. */
			cf->step  = CY_EVENT_STEP_IDLE  ;
		}

		return 0;
	}

	b ->off          += readed;
	b ->buf[b->off]   = 0     ;
	cf->eof           = 0     ;
	return 1;
}

PHP_FUNCTION(cy_fd_free)
{
	DEBUG_PRINT_FUNC_START
	zval           *zentry;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"O", &zentry, cy_fd_class_entry) == FAILURE)
	{
		RETURN_BOOL(0);
	}

	zval_ptr_dtor(&zentry);
}

PHP_FUNCTION(cy_fd_set_opt)
{
	cy_fd_t        *cf            ;
	zval           *zentry        ;
	const char     *key           ;
	int             key_len       ;
	long            val           ;

	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"Osl", &zentry, cy_fd_class_entry, &key, &key_len, &val) == FAILURE) {
		RETURN_BOOL(0);
	}

	cf = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr;
	if(strcmp("keepalive", key) == 0)
		cf->keepalive = val;

	if(strcmp("timeout", key) == 0)
		cf->timeout   = val;

	if(strcmp("mask", key) == 0)
		cf->mask      = val;

	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_fd_on)
{
	DEBUG_PRINT_FUNC_START
	cy_cb_params_t *fcb           ;
	cy_fd_t        *cf            ;
	long            type          ;

	zval           *zentry        ;
	zval           *cb            ;
	zval           *params = NULL ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
				"Olz|z", &zentry, cy_fd_class_entry, &type, &cb, &params) == FAILURE) {
		RETURN_BOOL(0);
	}

	if(type < 0 || CY_ON_MAX-1 < type)
	{
		zend_error(E_WARNING, "Invalid register function type(%ld). range is[%d - %d]", type, CY_ON_IDLE, CY_ON_END);
		RETURN_BOOL(0);
	}

	cf      = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr;
	fcb     = &cf->zcallbacks[type];

	cy_fd_set_zcallback(fcb, cb, params);
	RETURN_BOOL(1);
}

PHP_FUNCTION(cy_fd_construct)
{
	zval       *zentry            ;
	cy_object_base_t *o                 ;
	cy_fd_t    *cf                ;
	int         fd                ;
	long       type      = 0      ;
	long       timeout   = INT_MAX;

	/* zresource, zsession : params from input */
	zval       *zresource         ;
	zval       *zsession = NULL   ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(),
			"Oz|zll", &zentry, cy_fd_class_entry, &zresource, &zsession, &type, &timeout) == FAILURE)
	{
		return;
	}

	if (Z_TYPE_P(zresource) != IS_RESOURCE)
	{
		return;
	}

	php_stream *stream;
	if (ZEND_FETCH_RESOURCE_NO_RETURN(stream, php_stream *, &zresource, -1, NULL, php_file_le_stream()))
	{
		if (php_stream_cast(stream, PHP_STREAM_AS_FD_FOR_SELECT | PHP_STREAM_CAST_INTERNAL, (void* )&fd, 1) == SUCCESS || fd > 0)
		{
			goto done;
		}
	}

	return;

done:
	if(zsession)
		add_property_zval(zentry , "session"         , zsession      );

	/* make return_value as cy_fd_t type and add zentry ptr. */
	add_property_zval        (zentry , "client"          , zresource     );
	add_property_long        (zentry , "fd"              , fd            );

	cf           = cy_fd_init  (zentry ,   fd              , type          , timeout    );
	o            = zend_object_store_get_object(zentry  TSRMLS_CC          );
	o->ptr       = cf;
	o->free_func = cy_fd_free;
}

PHP_FUNCTION(cy_fd_set_handler)
{
	cy_fd_t             *cf     ;
	cy_fd_base_t *base   ;
	cy_cb_params_t      *fcb    ;
	zval                *object ;
	zval                *zentry ;
	zval                *cb1    ;
	zval                *cb2    ;
	zval                *cb     ;
	cy_cb_hook_func_t   *call   ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ozzz",
				&zentry, cy_fd_class_entry, &object, &cb1, &cb2) == FAILURE)
	{
		RETURN_LONG(-1);
	}

	/* get cy_fd_t object */
	cf                    = ((cy_object_base_t*)zend_object_store_get_object(zentry TSRMLS_CC))->ptr  ;

	// get cy_object_base_t
	cy_object_base_t  *o  = zend_object_store_get_object(object TSRMLS_CC);
	if(!o)
	{
		RETURN_LONG(-2);
	}

	// TODO: may coredump on error type.
	// need some security check.
	base                    = o->ptr                          ;
	base  ->cf              = cf                              ;
	cf    ->hook            = base                            ;
	base  ->buf             = cf     ->base ->buf             ;
	if(cf->type == 0)
	{
		fcb                     = &cf->ccallbacks[CY_ON_REQUEST  ];
		cy_fd_set_ccallback(fcb , base->send_cb , cb1, object    );

		fcb                     = &cf->ccallbacks[CY_ON_RESPONSE ];
		cy_fd_set_ccallback(fcb , base->recv_cb , cb2, object    );
	}
	else
	{	
		fcb                     = &cf->ccallbacks[CY_ON_REQUEST  ];
		cy_fd_set_ccallback(fcb , base->recv_cb , cb1, object    );

		fcb                     = &cf->ccallbacks[CY_ON_RESPONSE ];
		cy_fd_set_ccallback(fcb , base->send_cb , cb2, object    );
	}

	RETURN_BOOL(1);
}


