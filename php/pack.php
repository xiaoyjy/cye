<?php

$inputs1 = array(
        array(CY_TYPE_UINT32, 0x01),
        array(CY_TYPE_UINT32, 0x02),
        array(CY_TYPE_UINT32, 0x03),
);


$inputs = array(
        array(CY_TYPE_UINT8 , 0x06),
        array(CY_TYPE_UINT32, 0x01),
        array(CY_TYPE_UINT32, 0x02),
        array(CY_TYPE_UINT32, 0x03),
	array(CY_TYPE_OBJECT, $inputs1)
);

$config = array(
        array(CY_TYPE_UINT8 , 'count'),
        array(CY_TYPE_ARRAY , 'array', CY_TYPE_UINT32, 'count')
);

print_r(cy_unpack(cy_pack($inputs), $config));

?>
