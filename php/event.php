<?php

function send_req($c, $string)
{
	$r = fwrite($c->client, $string);
	return 0;
}

function get_connection($c)
{
	var_dump($c);
	return 0;
}

function read_data($c)
{
	$r = fgets($c->client, 8192);
	if($r == false)
	{
		return -1;
	}

	if(trim($r) == "")
	{
		return 0;
	}

	return 1;
}

function do_error($c, $error)
{
	echo "do_error\n";
	var_dump($c, $error);
	return 0;
}

$loop    = new cy_loop(128);
$session = "I'm Session";

$client = stream_socket_client("tcp://127.0.0.1:10000", $errno, $error, 2.5, STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_CONNECT);
$fd     = new cy_fd($client, $session);

cy_fd_set_opt($fd, "keepalive", 1);

cy_fd_on($fd, CY_ON_CONNECT, "get_connection");

$fd->on(CY_ON_WRITEABLE, "send_req"      , "cccccccccccccccccc");
$fd->on(CY_ON_READABLE , "read_data" );

cy_fd_on($fd, CY_ON_ERROR, "do_error");

echo "add client into loop\n";
$r = $loop->add($fd);

echo "run loop\n";
$r = $loop->run();

echo "end\n";

