<?php
function my_connection($c)
{
	var_dump($c);
	return 0;
}

function my_function($c, $o)
{
	var_dump($c, $o);

	if($c->session == "I'm Session")
	{
		$c->session = $o;
	}

	return 0;
}

function my_error($c, $o, $r)
{
	var_dump($c, $o, $r);

	echo "my_error end\n";
	return 0;
}


$loop    = new cy_loop(128);
$session = "I'm Session";

$client = stream_socket_client("tcp://127.0.0.1:10000", $errno, $error, 2.5, STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_CONNECT);
//$client = stream_socket_client("tcp://115.28.212.132:10000", $errno, $error, 2.5, STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_CONNECT);
$fd     = new cy_fd($client, $session, 0);
$loop->add ($fd);


$fd->on(CY_ON_CONNECT, "my_connection");
$fd->on(CY_ON_ERROR  , "my_error"     );

$http   = new cy_http_req();
$http->recv($fd, "my_function");

$loop->run ();
echo "end\n";

