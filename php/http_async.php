<?php

function send_req($c, $string)
{
	echo "send_req ...\n";
	$r = fwrite($c->client, $string);
	return 0;
}

function get_connection($c)
{
	var_dump($c);
	return 0;
}

function read_data($c)
{
	$r = fread($c->client, 16*1024*1024);
	$c->session->str .= $r;
	return 0;
}

function on_timeout($c, $req)
{

}

function do_error($c, $error)
{
	echo "do_error\n";
	return 0;
}

function on_close($c, $aaa)
{
	echo "on close\n";
	fclose($c->client);
}

function on_ready($c, $args, $data)
{
	echo "on_ready\n";
	//var_dump($data);
}

$loop    = new cy_loop(128);
$session = new stdClass    ;
$session->str = "";
$session->end = 2 ;

$client = stream_socket_client("tcp://www.baidu.com:80", $errno, $error, 2.5, STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_CONNECT);
stream_set_blocking($client, 0);

$fd     = new cy_fd($client, $session, 0, 500000);

$fd->on(CY_ON_CONNECT  , "get_connection");
$fd->on(CY_ON_REQUEST  , "send_req"      , "GET / HTTP/1.1\r\nHost: www.baidu.com\r\nAccept: */*\r\nConnection: close\r\n\r\n");
$fd->on(CY_ON_READY    , "on_ready"      );
$fd->on(CY_ON_ERROR    , "do_error"      );
$fd->on(CY_ON_CLOSE    , "on_close"      );

$rsp = new cy_http_cli();

function on_request($fd, $args, $data)
{
	echo __FUNCTION__, "\n";
	fwrite($fd->client, "GET / HTTP/1.1\r\nHost: www.baidu.com\r\nAccept: */*\r\nConnection: close\r\n\r\n");
	return 0;
}

function on_response($fd, $args, $data)
{
	echo __FUNCTION__, "\n";
	return 0;
}

$fd->set_handler($rsp, 'on_request', 'on_response');

echo "add client into loop\n";
$r = $loop->add($fd);

echo "run loop\n";
$r = $loop->run();

echo "end\n";

