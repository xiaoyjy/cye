#ifdef HAVE_CONFIG_H
	#include "config.h"
#endif

#include <dlfcn.h>
#include <php.h>

#include "cy_fe.h"
#include "structs.h"
#include "ae/ae_cb.h"
#include "ae/anet.h"
#include "ae/ae.h"
#include "http.h"

/*
 * struct http_parser_settings {
      http_cb      on_message_begin;
      http_data_cb on_url;
      http_data_cb on_status;
      http_data_cb on_header_field;
      http_data_cb on_header_value;
      http_cb      on_headers_complete;
      http_data_cb on_body;
      http_cb      on_message_complete;
      http_cb      on_chunk_header;
      http_cb      on_chunk_complete;
   };
 */
static const http_parser_settings settings = {
	.on_message_begin    = http_request_begin              ,
	.on_url              = http_request_on_url             ,
	.on_status           = http_request_on_status          ,
	.on_header_field     = http_request_on_header_field    ,
	.on_header_value     = http_request_on_header_value    ,
	.on_headers_complete = http_request_on_headers_complete,
	.on_body             = http_request_on_body            ,
	.on_message_complete = http_request_on_message_complete,
};


int cy_http_recv_cb         (void* param);
int cy_http_send_cb         (void* param);

int cy_http_internal_action (void* param);
int cy_http_internal_recv   (void* param);
int cy_http_internal_exec   (void* param);
int cy_http_internal_error  (void *param);

void cy_http_client_free(void* ptr)
{
	DEBUG_PRINT_FUNC_START
	cy_fd_base_t        *base = ptr       ; 
	cy_http_client_t    *http = base->data;

	http_request_free_zval  (http->parser);


	// delete http_parser
	if(http->parser)
		efree(http->parser);

	efree(http);
	efree(base);
}

void cy_http_client_init(zval *object, long type)
{
	cy_fd_base_t           *base   ;
	cy_http_client_t       *http   ;
	http_parser            *parser ;
	cy_object_base_t       *o      ;

	o = zend_object_store_get_object(object TSRMLS_CC);
	if(!o->ptr)
	{
		/* instance cy_http_client_t */
		http   = emalloc(sizeof(cy_http_client_t  ));
		memset(http, 0 , sizeof(cy_http_client_t  ));

		parser = emalloc(sizeof(http_parser       ));
		parser->data          = http   ;
		http  ->parser        = parser ;

		/* instance cy_fd_base_t   */
		base = emalloc( sizeof(cy_fd_base_t       ));
		memset(base, 0, sizeof(cy_fd_base_t       ));
		base->data            = http                ;
		base->zentry          = object              ;
		base->recv_cb         = cy_http_recv_cb     ;
		base->send_cb         = cy_http_send_cb     ;

		http->base            = base                ;
		o   ->ptr             = base                ;
		o   ->free_func       = cy_http_client_free ;
	}
	else
	{
		base                  = o   ->ptr   ;
		http                  = base->data  ;
		parser                = http->parser;
	}

	/*
	if(type != -1)
	{
		base->buf             = emalloc (CY_BUF_INITIA_LEN + sizeof(cy_buf_token_t) + sizeof(size_t));
		memset(base->buf, 0, sizeof(cy_buf_token_t));
		base->buf  ->len      = CY_BUF_INITIA_LEN   ;
	}
	*/

	// init http parser as request.
	if(type == 0)
	{
		http->settings = cy_http_response_settings();
		http_parser_init (parser, HTTP_RESPONSE);		
	}
	else
	{
		http->settings = cy_http_request_settings();
		http_parser_init (parser, HTTP_REQUEST );
	}
}

/* if return = 1, not execute after call */
int cy_http_recv_cb(void* param)
{
	cy_cb_params_t       *p          = param              ;
	cy_fd_t              *cf         = p    ->cf          ;
	cy_fd_base_t         *base       = cf   ->hook        ;
	int                   ret                             ;

	/* read for network */
	ret = cy_http_internal_recv(param);

	/* errors or eof or connect close */
	if(ret <= 0)
	{
		return ret;
	}

	ret = cy_http_internal_exec(param);
	/* errors or eof or connect close */
	if(ret <= 0)
	{
		return ret;
	}

	return 1;
}

/* if return = 1, not execute after call */
int cy_http_send_cb(void* param)
{
	/* cy_cb_params_t { func, args, data, cb, cf, root, ptr } */
	cy_cb_params_t       *p          = param              ;
	cy_fd_t              *cf         = p    ->cf          ;
	int                   ret                             ;

	cy_ae_call_user_function_ex(cf, &cf->ccallbacks[CY_ON_RESPONSE]);

	ret = cy_http_internal_exec(param);
	if(ret <= 0)
	{
		return ret;
	}

	return 1;
}

int cy_http_internal_error(void *param)
{
	/* cy_cb_params_t { func, args, data, cb, cf, root, ptr } */
	cy_cb_params_t       *p          = param              ;
	cy_fd_t              *cf         = p    ->cf          ;
	cy_fd_base_t         *base       = cf   ->hook        ;
	cy_http_client_t     *c          = base ->data        ;
	cy_buf_token_t       *b          = base ->buf         ;
	http_parser          *parser     = c    ->parser      ;

	cy_cb_params_t *fcb  = &cf->zcallbacks[CY_ON_ERROR]   ;
	if(fcb->cb)
	{
		/*
		 * in normal situation, zrequest will be initialized at http_request_begin
		 * but http error may happen before http_request_begin. 
		 */
		if(!c->base->zrequest)
		{
			MAKE_STD_ZVAL (c->base->zrequest);
			object_init   (c->base->zrequest);
		}

		add_property_long   (c->base->zrequest, "errno"  ,                 parser->http_errno        );
		add_property_string (c->base->zrequest, "error"  , http_errno_name(parser->http_errno),  1   );
		fcb->args          = c->base->zrequest;
		cy_ae_call_user_function_ex(cf  , fcb);
	}
	else
	{
		zend_error(E_WARNING ,
				"http parse exec errno(%d) %s, set on error handle "
				"define CY_ON_ERROR handler disable this warning, eg:\n " 
				"\t$fd->on(CY_ON_ERROR, $callback); ",
				parser->http_errno, http_errno_name(parser->http_errno)); 
	}

	return -100*CY_ON_ERROR-1;
}

#define RELINK_TOKEN_INDEX(new_addr, old_addr) \
	for(int i = 0; i < CURRENT_TK_MAX; i++) \
{ \
	if(c->index[i].str) \
	c->index[i].str = (new_addr) + (c->index[i].str - (old_addr)); \
}

int cy_http_internal_exec  (void* param)
{
	/* cy_cb_params_t { func, args, data, cb, cf, root, ptr } */
	cy_cb_params_t       *p          = param              ;
	cy_fd_base_t         *base       = p    ->cf   ->hook ;
	cy_http_client_t     *c          = base ->data        ;
	cy_buf_token_t       *b          = base ->buf         ;
	http_parser          *parser     = c    ->parser      ;

	/* if b->end is set means the last request is finished,
	 * move lefted data to the begining of buffer
	 */
	if(b->end != 0)
	{
		if(b->off != b->end)
		{
			memmove(b->buf, b->buf + b->end, b->off - b->end);

			/* important!!! update client pionter to new new_ptr */
			RELINK_TOKEN_INDEX(b->buf      , b->buf + b->end);
		}

		b->parsed      = b->parsed - b->end    ;
		b->off         = b->off    - b->end    ;
		b->end         = 0                     ;
		b->buf[b->off] = 0                     ;
		DEBUG_PRINT("last request finished, left %d bytes: %s\n", b->off, b->buf);
	}

	if(b->parsed == b->off)
	{
		return 0;
	}

	b->parsed += http_parser_execute(parser, &settings, b->buf + b->parsed, b->off - b->parsed);
	if(parser->http_errno)
	{
		cy_http_internal_error(param);
		return -2;
	}

	return 1;
}

int cy_http_internal_recv  (void* param)
{
	/* in this function cf => cy_fd_t, c => client */
	cy_cb_params_t       *p          = param              ;
	cy_fd_base_t         *base       = p    ->cf   ->hook ;
	cy_http_client_t     *c          = base ->data        ;
	char                 *oldbuf     = base ->buf  ->buf  ;
	int                   ret                             ;

	ret = cy_fd_internal_recv(param);
	if(ret < 0)
	{
		return ret;
	}

	/* relink http_parser */
	if(oldbuf != base->buf->buf)
	{
		RELINK_TOKEN_INDEX(base->buf->buf, oldbuf);
	}

	return ret;
}

PHP_FUNCTION(cy_http_construct)
{
	DEBUG_PRINT("at %s:%d %s\n", __FILE__,  __LINE__, __FUNCTION__);

	/* type=0 client; type=1 server */
	long                    type   = -1;
	zval                   *object     ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l",
				&object, cy_http_class_entry, &type) == FAILURE)
	{
		zend_error(E_WARNING, "cy_http_construct paramters error.");
		return;
	}

	cy_http_client_init(object, type);
}

PHP_FUNCTION(cy_http_init)
{
	DEBUG_PRINT("at %s:%d %s\n", __FILE__,  __LINE__, __FUNCTION__);

	/* type=0 client; type=1 server */
	long                    type   = 0 ;
	zval                   *object     ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l",
				&object, cy_http_class_entry, &type) == FAILURE)
	{
		zend_error(E_WARNING, "cy_http_construct paramters error.");
		return;
	}

	cy_http_client_init(object, type);
}

PHP_FUNCTION(cy_http_parse)
{
	cy_fd_base_t        *base   ;
	cy_http_client_t    *http   ;
	zval                *object ;
	const char          *str    ;
	int                  len    ;
	http_parser         *parser ;
	long                 type   ;
	if(zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Osl",
				&object, cy_http_class_entry, &str, &len, &type) == FAILURE)
	{
		return;
	}

	cy_http_client_init(object, type);

	base          = ((cy_object_base_t*)zend_object_store_get_object(object TSRMLS_CC))->ptr  ;
	http          = base->data                                                                ;
	parser        = http->parser                                                              ;

	zval *result;
	http_parser_execute(parser, http->settings, str, len);

	result = (type == 0) ? base->zresponse : base->zrequest;
	if(!result)
	{
		RETURN_LONG(parser->http_errno);
	}

	add_property_long(result  , "errno",  parser->http_errno);
	*return_value  = *result;
}

